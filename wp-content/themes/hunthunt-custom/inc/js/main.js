
function initMap(centerMapObj) {
        var mapDiv = document.getElementById('map');
		var currentMap = centerMapObj ? centerMapObj : {lat: -33.7755442, lng: 151.1197693};
        var map = new google.maps.Map(mapDiv, {
            center: currentMap,
            zoom: 15,
			scrollwheel:  false
        });
		
		map.set('styles', 
		[{"featureType":"administrative","elementType":"all","stylers":[{"hue":"#000000"},{"lightness":-100},{"visibility":"off"}]},
		{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"lightness":"3"},{"weight":"0.31"},{"saturation":"38"}]},
		{"featureType":"administrative","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":"88"},{"gamma":"0.00"},{"saturation":"8"},{"weight":"0.18"},{"color":"#c40808"}]},
		{"featureType":"administrative","elementType":"labels.text","stylers":[{"lightness":"41"}]},
		{"featureType":"administrative","elementType":"labels.icon","stylers":[{"visibility":"off"},{"lightness":"0"}]},
		{"featureType":"administrative.country","elementType":"geometry","stylers":[{"saturation":"-35"},{"gamma":"2.41"},{"lightness":"18"},{"visibility":"on"}]},
		{"featureType":"administrative.country","elementType":"labels","stylers":[{"saturation":"-53"},{"lightness":"-32"}]},
		{"featureType":"administrative.province","elementType":"geometry","stylers":[{"saturation":"-36"}]},
		{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"saturation":"29"},{"lightness":"69"}]},
		{"featureType":"administrative.province","elementType":"labels.text","stylers":[{"color":"#c84136"}]},
		{"featureType":"administrative.locality","elementType":"labels","stylers":[{"color":"#825151"},{"visibility":"on"}]},
		{"featureType":"administrative.neighborhood","elementType":"labels.text","stylers":[{"color":"#e06055"},{"visibility":"on"},{"lightness":"36"},{"saturation":"-50"}]},
		{"featureType":"landscape","elementType":"all","stylers":[{"lightness":"-30"},{"saturation":"80"},{"gamma":"2.00"}]},
		{"featureType":"landscape","elementType":"geometry","stylers":[{"lightness":"57"},{"saturation":"-19"},{"hue":"#ff3400"}]},
		{"featureType":"landscape","elementType":"labels","stylers":[{"visibility":"off"}]},
		{"featureType":"landscape.man_made","elementType":"all","stylers":[{"saturation":"-24"},{"lightness":"20"}]},
		{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"on"},{"color":"#e06b55"},{"lightness":"58"},{"saturation":"-18"},{"gamma":"1.21"}]},
		{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},
		{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"simplified"},{"saturation":"0"},{"color":"#f79a9a"},{"lightness":"6"}]},
		{"featureType":"road","elementType":"labels","stylers":[{"visibility":"simplified"}]},
		{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},
		{"featureType":"road.local","elementType":"all","stylers":[{"saturation":-100},{"lightness":100},{"visibility":"off"}]},
		{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"on"}]},
		{"featureType":"road.local","elementType":"labels","stylers":[{"visibility":"simplified"},{"color":"#7e7373"}]},
		{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},
		{"featureType":"transit","elementType":"labels","stylers":[{"lightness":-100},{"visibility":"off"},{"color":"#fdb8b8"}]},
		{"featureType":"water","elementType":"geometry","stylers":[{"saturation":"-34"},{"lightness":"28"},{"visibility":"on"},{"color":"#eccccc"}]},
		{"featureType":"water","elementType":"labels","stylers":[{"hue":"#000000"},{"saturation":100},{"lightness":100},{"visibility":"off"}]}]);
		
		  var marker = new google.maps.Marker({
			position: new google.maps.LatLng(currentMap.lat, currentMap.lng),
			map: map
		  });
	 }
	 
	 $(document).ready(function(){
		
			$( ".map-overlay" ).on( "click", function() {
				$(this).css('pointer-events','none');
			});
			
			$( function() {
			// run the currently selected effect
			function runEffect() {
			 
			  // Run the effect
			  $( "#open-contact-panel" ).hide();
			  $( "#contact-panel" ).toggle( "slide", {direction: 'right'}, 200 );
			};
		 
			// Set effect from select menu value
			$( "#open-contact-panel" ).on( "click", function() {
			  runEffect();
			});
			$( "#close-contact-panel" ).on( "click", function() {
			  runEffect();
			  window.setTimeout(function(){ 
			  $( "#open-contact-panel" ).show();
			  }, 200);
			  
			});
		  } );
		  /*
			$('#close-preferred-office-btn').on('click',function(){
				$('.preferred-office.panel').addClass('closed');
				$('#close-preferred-office-btn').hide();
				$('#open-preferred-office-btn').show();
			});
			$('#open-preferred-office-btn').on('click',function(){
				$('.preferred-office.panel').removeClass('closed');
				$('#close-preferred-office-btn').show();
				$('#open-preferred-office-btn').hide();
			});
            */
			/*if($(window).width()<768){
				$('#close-preferred-office-btn').trigger('click');
			}*/
         /*
			$('.office-nav').on('click','a',function(ev){
				 ev.preventDefault();
				 $('.office-nav a').removeClass('active');
				 $(this).addClass('active');
				 $('[data-location="'+$(this).data('location')+'"]' ).addClass('active');
				 initMap(centerMap[$(this).data('location')][0]);
				 setAddress(centerMap[$(this).data('location')][1],$(this).data('location'))
			 });
			*/
			 var d = new Date();
			 var n = d.getFullYear();
			 $('#current-year').text(n);
			 
			 if($(window).width()<992){
			   $('.dropdown-submenu > a').on("click", function(e){
				$(this).next('ul').toggle();
				e.stopPropagation();
				e.preventDefault();
			  });
			 }
         
      
			$('#cat').on('change', function(){
         		window.location = '/publications?cat='+$('#cat').val();
         	});
         	
	 });
	 
