/*
var centerMap = {
		sydney:[{lat: -33.861909,lng: 151.209993},{level:'Gateway, Level 13',street:'1 Macquarie Place',city:'Sydney NSW 2000',mapLink:'https://www.google.com/maps/place/Hunt+%26+Hunt/@-33.8619011,151.2078059,17z/data=!3m1!4b1!4m5!3m4!1s0x6b12ae69e02614d7:0x795f6272afbda78d!8m2!3d-33.8619011!4d151.2099946?hl=en',phone:'+61 2 9391 3000',fax:'+61 2 9391 3099',email:'info@hunthunt.com.au'}],
		'north ryde':[{lat: -33.7755442, lng: 151.1197693},{level:'Level 2',street:'1 Innovation Road (cnr Herring Road)',city:'North Ryde NSW 2113',mapLink:'http://maps.google.com.au/maps?q=Hunt+%26+Hunt+Lawyers,+Innovation+Road,+North+Ryde,+New+South+Wales&hl=en&ll=-33.771228,151.119776&spn=0.050372,0.068579&sll=-25.335448,135.745076&sspn=54.381985,70.224609&oq=Hunt+%26+Hunt+north+ryde&hq=Hunt+%26+Hunt+Lawyers,+Innovation+Road,&hnear=North+Ryde+New+South+Wales&t=m&z=14&iwloc=A',phone:' +61 2 9804 5700',fax:'+61 2 9804 5799',email:'info@huntnsw.com.au'}],
		melbourne:[{lat: -37.8145944, lng: 144.9626267},{level:'Level 26',street:'385 Bourke Street',city:'Melbourne VIC 3000 ',mapLink:'http://maps.google.com.au/maps?q=Hunt+%26+Hunt+Lawyers,+Bourke+Street,+Melbourne,+Victoria&hl=en&ll=-37.814124,144.963076&spn=0.011968,0.017145&sll=-25.335448,135.745076&sspn=54.381985,70.224609&oq=Hunt+%26+Hunt+&hq=Hunt+%26+Hunt+Lawyers,&hnear=Bourke+St,+Melbourne+Victoria&t=m&z=16&iwloc=A',phone:'+61 3 8602 9200',fax:'+61 3 8602 9299',email:'info@huntvic.com.au'}],
		hobart:[{lat: -42.881573, lng: 147.3311023},{level:'Level 9',street:'85 Macquarie Street',city:'Hobart TAS 7000',mapLink:'https://www.google.com.au/maps/place/Hunt+%26+Hunt/@-42.881572,147.3290343,17z/data=!3m1!4b1!4m5!3m4!1s0xaa6e75847bba076d:0x6ae4a457104fa7f1!8m2!3d-42.881572!4d147.331223',phone:'(03) 6210 6200',fax:'1300 377 441',email:'http://www.hunttas.com.au/'}],
		brisbane:[{lat: -27.466298, lng: 153.0299186},{level:'Level 12',street:'110 Eagle Street',city:'Brisbane QLD 4000 ',mapLink:'https://maps.google.com.au/maps?q=Nicholsons+Solicitors,+Level+12,+110+Eagle+Street,+Brisbane,+Queensland,+4000&hl=en&sll=-27.49299,153.00771&sspn=0.118318,0.216465&t=m&z=17&iwloc=A',phone:' +61 7 3226 3944',fax:'+61 7 3221 3756',email:'http://www.nicholsons.com.au/'}],
		perth:[{lat: -31.959291, lng: 115.8647584},{level:'Culshaw Miller Lawyers',street:'Level 1,16 St Georges Terrace',city:'Perth WA 6000',mapLink:'http://maps.google.com.au/maps?q=Culshaw+Miller+Lawyers,+Victoria+Avenue,+Perth,+Western+Australia&hl=en&ll=-31.958725,115.864767&spn=0.006426,0.008572&sll=-25.335448,135.745076&sspn=54.381985,70.224609&oq=Culshaw+miller&hq=Culshaw+Miller+Lawyers,&hnear=Victoria+Ave,+Perth+Western+Australia+6000&t=m&z=17&iwloc=A',phone:'+61 8 9488 1300',fax:'+61 8 9488 1395',email:'info@hunthunt.com.au'}],
		darwin:[{lat: -12.4634334, lng: 130.8437129},{level:'Level 2',street:'13 Cavenagh Street',city:'Darwin NT 0800',mapLink:'http://maps.google.com.au/maps?q=Hunt+%26+Hunt+Lawyers,+Cavenagh+Street,+Darwin,+Northern+Territory&hl=en&ll=-12.463218,130.843381&spn=0.007396,0.008572&sll=-25.335448,135.745076&sspn=54.381985,70.224609&oq=Hunt+%26+Hunt+&hq=Hunt+%26+Hunt+Lawyers,&hnear=Cavenagh+St,+Darwin+Northern+Territory+0800&t=m&z=17&iwloc=A',phone:'+61 8 8924 2600',fax:' +61 8 8941 0012',email:' info@hunthunt.com.au'}],
		shangai:[{lat: 31.2117724, lng: 121.4309393},{level:'Room 701, Summit Centre',street:"1088 Yan'an Xi Road",city:'Shanghai China 200052 ',mapLink:'http://maps.google.com.au/maps?q=summit+centre&hl=en&ie=UTF8&ll=31.213682,121.430855&spn=0.103649,0.137157&sll=31.205399,121.444468&sspn=0.059984,0.070168&near=1200%E5%8F%B7+Yan%27an+East+Road,+Huangpu,+Shanghai,+China+%28Shangzi+Accounting+Firm%29&geocode=Cfl412gpCe2tFRR53AEduIY9ByGNMEyLY2f4pQ&dq=Yan%27an+Xi+Road+loc:+Shanghai,+China&hq=summit+centre&t=m&z=13&iwloc=A',phone:'+86 21 6249 3543',fax:'+86 21 6249 3545',email:'info@huntnsw.com.au'}],
	};
function setAddress(addressObj,locationName){
	var contactPanelHtml = '';
	contactPanelHtml+='<h3>OUR '+locationName.toUpperCase()+' ADDRESS</h3>';
	contactPanelHtml+=	addressObj.level+'<br />';
	contactPanelHtml+=	addressObj.street+'<br />';
	contactPanelHtml+=	addressObj.city+'<br />';
	contactPanelHtml+=	'<a href="'+addressObj.mapLink+'" target="_blank">View map in Google</a><br /><br />';
	contactPanelHtml+='<p><strong>T :</strong> '+addressObj.phone+'<br />';
	contactPanelHtml+=	'<strong>F :</strong> '+addressObj.fax+'<br />';
	contactPanelHtml+=	'<strong>E :</strong> '+addressObj.email+'</p>';
	$('#address-data').empty();
	$('#address-data').html(contactPanelHtml);
}
*/

	 
	 $(document).ready(function(){
		/* $('.carousel').carousel({
			  interval: 5000
			});
            */
			
			 
		var bgImageArray = [ "divorce-separation","settlements", "spousal-maintenance", "parenting", "financial-agreements", "wills-estate" ],
		base = "wp-content/themes/hunthunt-custom/inc/img/",
		secs = 5;
		bgImageArray.forEach(function(img){
			if($(window).width()<768){
				new Image().src = base + img+'-sml.jpg'; 
			}else{
				new Image().src = base + img+'.jpg'; 
			}
		});
         /*
			 function backgroundSequence() {
			//window.clearTimeout();
				var k = 0;
				for (var i = 0; i < bgImageArray.length; i++) {
					setTimeout(function(){
					document.getElementsByTagName('header')[0].style.background = "url(" + base + bgImageArray[k] + ") top center no-repeat";
					if($(window).width()<768){
					document.getElementsByTagName('header')[0].style.backgroundSize ="cover";
					}else{
						document.getElementsByTagName('header')[0].style.backgroundSize ="100%";
					}
						if ((k + 1) === bgImageArray.length) {
							setTimeout(function() {
								backgroundSequence() }, (secs * 1000))
						} else { k++; }
					}, (secs * 1000) * i) 
				}
			}
			//backgroundSequence();
			*/
			function backgroundTrigger(k) {
									
				//document.getElementsByTagName('header')[0].style.transitionProperty ="background-image";
				//document.getElementsByTagName('header')[0].style.transitionDuration ="5s";
				if($(window).width()<768){
					document.getElementsByTagName('header')[0].style.background = "url(" + base + bgImageArray[k] + "-sml.jpg) top center no-repeat";
					document.getElementsByTagName('header')[0].style.backgroundSize ="cover";
				}else{
					document.getElementsByTagName('header')[0].style.background = "url(" + base + bgImageArray[k] + ".jpg) top center no-repeat";
					document.getElementsByTagName('header')[0].style.backgroundSize ="100%";  
				}										
			}
			
			
			$('.home-carousel').slick({
				  autoplay: true,
				  autoplaySpeed: 4000,
				  dots: true,
				  infinite: true,
				  fade: true,
				  cssEase: 'linear',
				  arrows:false,
				  pauseOnHover:false,
			  });
			  
			$('.home-carousel').on('beforeChange', function( event, slick, currentSlide, nextSlide){
					backgroundTrigger(nextSlide);
			});
	 });