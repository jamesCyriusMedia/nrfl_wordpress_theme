<!DOCTYPE html>
<!-- saved from url=(0059)https://hunthunt.worldsecuresystems.com/about-us/who-we-are -->
<html class="no-js" lang="en">
<!--<![endif]-->
<!-- BC_OBNW -->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Hunt & Hunt Lawyers North Ryde Macquarie Park | Family, Property & Commercial Law</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/main.css">
    <link rel="stylesheet" type="text/css" href="/css/slick.css">

    <meta name="description" content="When you engage Hunt &amp; Hunt Lawyers, you access a vast pool of resourceful lawyers who are passionate about the law and providing high-quality advice">
    <meta name="keywords" content="Hunt &amp; Hunt Lawyers, who we are, about us, our people, what we do, services, sectors, government, not-for-profit, insurance, business, private clients, national firm, locations, join us, aged care, agribusiness, alpine, automotive, banking and finance, building and construction, property, customs, china advisory ">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
</head>

<body>

    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->    

    <header class="internal-header-5">
		<?php include "block/header.php" ?>
    </header>

    <section class="preferred-office panel">
        <div class="container">
            <h1 class="red-bar-heading">Coming soon</h1>
        </div>
    </section>
    <section class="main-content">
        <div class="container">            
            
        </div>
    </section>
    
	
    <section class="memberships">
        <?php include "block/about-memberships.php" ?>
    </section>
    <footer>
        
		<?php include "block/footer-menu.php" ?>
		<?php include "block/footer-ribborn.php" ?>

    </footer>
    <script src="/js/jquery-1.11.2.min.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/js/slick.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/main.js"></script>
    

</body>

</html>