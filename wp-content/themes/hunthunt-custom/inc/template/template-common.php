<!DOCTYPE html>
<!-- saved from url=(0059)https://hunthunt.worldsecuresystems.com/about-us/who-we-are -->
<html class="no-js" lang="en">
<!--<![endif]-->
<!-- BC_OBNW -->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Hunt & Hunt Lawyers North Ryde Macquarie Park | Family, Property & Commercial Law</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/main.css">
    <link rel="stylesheet" type="text/css" href="/css/slick.css">

    <meta name="description" content="When you engage Hunt &amp; Hunt Lawyers, you access a vast pool of resourceful lawyers who are passionate about the law and providing high-quality advice">
    <meta name="keywords" content="Hunt &amp; Hunt Lawyers, who we are, about us, our people, what we do, services, sectors, government, not-for-profit, insurance, business, private clients, national firm, locations, join us, aged care, agribusiness, alpine, automotive, banking and finance, building and construction, property, customs, china advisory ">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
</head>

<body>

    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <?php 

        switch ($page) {
            // About section
            case "who-we-are":
                $title = "WHO WE ARE";
                $block = "block/about-who-we-are.php";
                $group_page = "about";
                break;
            case "our-history":
                $title = "OUR HISTORY";
                $block = "block/about-our-history.php";
                $group_page = "about";
                break;
            case "our-offices":
                $title = "OUR OFFICES";
                $block = "block/about-our-offices.php";
                $group_page = "about";
                break;
            case "corporate-social-responsibility":
                $title = "CORPORATE SOCIAL RESPONSIBILITY";
                $block = "block/about-corporate-social-responsibility.php";
                $group_page = "about";
                break;
            case "community":
                $title = "COMMUNITY";
                $block = "block/about-community.php";
                $group_page = "about";
                break;
            case "careers":
                $title = "CAREERS";
                $block = "block/about-careers.php";
                $group_page = "about";
                break;  
                
            // Services section
            case "services-asset":
                $title = "ASSET PROTECTION AND SUCCESSION PLANNING";
                $block = "block/services-asset.php";
                $group_page = "services";
                break;
            
            case "services-china":
                $title = "China advisory";
                $block = "block/services-china.php";
                $group_page = "services";
                break;

            case "services-chinese":
                $title = "中国咨询服务";
                $block = "block/services-chinese.php";
                $group_page = "services";
                break;

            case "services-corporate":
                $title = "Corporate and commercial";
                $block = "block/services-corporate.php";
                $group_page = "services";
                break;

            case "services-competition":
                $title = "Competition and consumer law";
                $block = "block/services-competition.php";
                $group_page = "services";
                break;

            case "services-employment":
                $title = "Employment and workplace relations";
                $block = "block/services-employment.php";
                $group_page = "services";
                break;

            case "services-environment":
                $title = "Environment and planning";
                $block = "block/services-environment.php";
                $group_page = "services";
                break;

            case "services-family":
                $title = "Family law";
                $block = "block/services-family.php";
                $group_page = "services";
                break;

            case "services-intellectual":
                $title = "Intellectual property";
                $block = "block/services-intellectual.php";
                $group_page = "services";
                break;

            case "services-insolvency":
                $title = "Insolvency and restructuring";
                $block = "block/services-insolvency.php";
                $group_page = "services";
                break;
            
            case "services-litigation":
                $title = "Litigation and dispute resolution";
                $block = "block/services-litigation.php";
                $group_page = "services";
                break;

            case "services-mergers":
                $title = "Mergers and acquisitions";
                $block = "block/services-mergers.php";
                $group_page = "services";
                break;

            case "services-property":
                $title = "Property law";
                $block = "block/services-property.php";
                $group_page = "services";
                break;

            case "services-wills":
                $title = "Wills and estates planning";
                $block = "block/services-wills.php";
                $group_page = "services";
                break;


            case "sectors-aged":
                $title = "Aged Care";
                $block = "block/sectors-aged.php";
                $group_page = "sectors";
                break;

            case "sectors-agribusiness":
                $title = "Agribusiness";
                $block = "block/sectors-agribusiness.php";
                $group_page = "sectors";
                break;

            case "sectors-alpine":
                $title = "Alpine";
                $block = "block/sectors-alpine.php";
                $group_page = "sectors";
                break;

            case "sectors-automotive":
                $title = "Automotive";
                $block = "block/sectors-automotive.php";
                $group_page = "sectors";
                break;

            case "sectors-banking":
                $title = "Banking and Finance";
                $block = "block/sectors-banking.php";
                $group_page = "sectors";
                break;

            case "sectors-building":
                $title = "Building and Construction";
                $block = "block/sectors-building.php";
                $group_page = "sectors";
                break;

            case "sectors-business":
                $title = "Business";
                $block = "block/sectors-business.php";
                $group_page = "sectors";
                break;

            case "sectors-customs":
                $title = "Customs and Global Trade";
                $block = "block/sectors-customs.php";
                $group_page = "sectors";
                break;

            case "sectors-education":
                $title = "Education";
                $block = "block/sectors-education.php";
                $group_page = "sectors";
                break;

            case "sectors-energy":
                $title = "Energy and Resources";
                $block = "block/sectors-energy.php";
                $group_page = "sectors";
                break;

            case "sectors-government":
                $title = "Government";
                $block = "block/sectors-government.php";
                $group_page = "sectors";
                break;


            case "sectors-health":
                $title = "Health";
                $block = "block/sectors-health.php";
                $group_page = "sectors";
                break;

            case "sectors-insurance":
                $title = "Insurance";
                $block = "block/sectors-insurance.php";
                $group_page = "sectors";
                break;

            case "sectors-not-for-profit":
                $title = "Not for profit";
                $block = "block/sectors-not-for-profit.php";
                $group_page = "sectors";
                break;

            case "sectors-private":
                $title = "Private Clients";
                $block = "block/sectors-private.php";
                $group_page = "sectors";
                break;            
        }


        switch ($group_page) {

            case "about":
                $menu = "block/about-menu.php";
                $header_class = "internal-header";
                break;
                
            case "services":
                $menu = "block/services-menu.php";
                $header_class = "internal-header-4";
                break;

            case "sectors":
                $menu = "block/sectors-menu.php";
                $header_class = "internal-header-4";
                break;
            

        };

        

    ?>
    <header class="<?php echo $header_class ?>">
		<?php include "block/header.php" ?>
    </header>

    <section class="preferred-office panel">
        <div class="container">
            <h1 class="red-bar-heading"><?php echo $title; ?></h1>
        </div>
    </section>
    <section class="main-content">
        <div class="container">
            <div class="row">

                <div class="col-sm-8">
                    <?php include $block; ?>
                </div>
                <div class="col-sm-4">
                    <?php include $menu; ?>
                </div>
            </div>
        </div>
    </section>
    <section class="map-zone">
       <?php include "block/about-map.php" ?>
	</section>
	
    <section class="memberships">
        <?php include "block/about-memberships.php" ?>
    </section>
    <footer>
        
		<?php include "block/footer-menu.php" ?>
		<?php include "block/footer-ribborn.php" ?>

    </footer>
    <script src="/js/jquery-1.11.2.min.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/js/slick.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/main.js"></script>
    

</body>

</html>