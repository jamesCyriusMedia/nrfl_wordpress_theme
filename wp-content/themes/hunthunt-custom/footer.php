<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Hunt_Hunt_Custom
 * @since Hunt Hunt Custom 1.0
 */

?>
<section class="map-zone">
    <?php //include "block/home-map.php" ?>
    <?php get_template_part( 'template-parts/block/home-map'); ?>
</section>

<section class="memberships">
    <?php //include "block/home-memberships.php" ?>
    <?php get_template_part( 'template-parts/block/home-memberships'); ?>
</section>
<footer>
<?php //include "block/footer-menu.php" ?>
<?php get_template_part( 'template-parts/block/footer-menu'); ?>
<?php //include "block/footer-ribborn.php" ?>
<?php get_template_part( 'template-parts/block/footer-ribborn'); ?>
</footer>
    <!-- <script src="js/jquery-1.11.2.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/slick.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/home-carousel.js"></script> -->
    
    <?php wp_footer(); ?>
</body>
</html>