<?php
/**
 * Hunt Hunt Custom functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Hunt_Hunt_Custom
 * @since Hunt Hunt Custom 1.0
 */

function hunthunt_theme_setup(){
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'title-tag' );
}
add_action('after_setup_theme','hunthunt_theme_setup');

function hunthunt_register_styles() {
	

	// Add other CSS.
    wp_enqueue_style( 'hunthunt-bootstrap-style', get_template_directory_uri() . '/inc/css/bootstrap.min.css', null );
    wp_enqueue_style( 'hunthunt-bootstrap-theme-style', get_template_directory_uri() . '/inc/css/bootstrap-theme.min.css', array('hunthunt-bootstrap-style') );
    wp_enqueue_style( 'hunthunt-modules-style', get_template_directory_uri() . '/inc/css/modulestylesheets.css', null );
    wp_enqueue_style( 'hunthunt-slick-style', get_template_directory_uri() . '/inc/css/slick.css', null );

    wp_enqueue_style( 'hunthunt-style', get_stylesheet_uri(), array());
}
add_action( 'wp_enqueue_scripts', 'hunthunt_register_styles' );

function twentytwenty_register_scripts() {
    wp_enqueue_script( 'hunthunt-jquery-js', get_template_directory_uri() . '/inc/js/jquery-1.11.2.min.js', array(), true);
    wp_enqueue_script( 'hunthunt-jquery-ui-js', get_template_directory_uri() . '/inc/js/jquery-ui.min.js', array(), true);
    // wp_enqueue_script( 'hunthunt-slickmin-js', get_template_directory_uri() . '/inc/js/slick.min.js', array(), true);
    wp_enqueue_script( 'hunthunt-bootstrap-js', get_template_directory_uri() . '/inc/js/bootstrap.min.js', array(), true);
    wp_enqueue_script( 'hunthunt-main-js', get_template_directory_uri() . '/inc/js/main.js', array(), true);
    if(is_front_page()){
        wp_enqueue_script( 'hunthunt-home-carousel-js', get_template_directory_uri() . '/inc/js/home-carousel.js', array(), true);
    }


    // wp_enqueue_script( 'hunthunt-common-js', get_template_directory_uri() . '/inc/js/common.js', array());
    // wp_enqueue_script( 'hunthunt-util-js', get_template_directory_uri() . '/inc/js/util.js', array());
    // wp_enqueue_script( 'hunthunt-map-js', get_template_directory_uri() . '/inc/js/map.js', array());
    // wp_enqueue_script( 'hunthunt-marker-js', get_template_directory_uri() . '/inc/js/marker.js', array());
    // wp_enqueue_script( 'hunthunt-stats-js', get_template_directory_uri() . '/inc/js/stats.js', array());
    // wp_enqueue_script( 'hunthunt-onion-js', get_template_directory_uri() . '/inc/js/onion.js', array());
    // wp_enqueue_script( 'hunthunt-controls-js', get_template_directory_uri() . '/inc/js/controls.js', array());
    wp_enqueue_script( 'hunthunt-slick-js', get_template_directory_uri() . '/inc/js/slick.min.js', array());


    // wp_enqueue_script( 'hunthunt-modernizr-js', get_template_directory_uri() . '/inc/js/modernizr-2.8.3-respond-1.4.2.min.js', array());
    // wp_enqueue_script( 'hunthunt-npm-js', get_template_directory_uri() . '/inc/js/npm.js', array());
	// wp_script_add_data( 'twentytwenty-js', 'async', true );

}

add_action( 'wp_enqueue_scripts', 'twentytwenty_register_scripts' );

function et_add_viewport_meta(){
	echo '<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />';
}
add_action( 'wp_head', 'et_add_viewport_meta' );