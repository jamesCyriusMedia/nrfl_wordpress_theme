<?php
/**
 * Header file for the Hunt Hunt Custom theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Hunt_Hunt
 * @since Hunt Hunt 1.0
 */

?>
<!DOCTYPE html>
<!-- saved from url=(0040)https://hunthunt.worldsecuresystems.com/ -->
<html class="no-js" lang="en">
<!--<![endif]-->
<!-- BC_OBNW -->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
        .dismissButton {
            background-color: #fff;
            border: 1px solid #dadce0;
            color: #1a73e8;
            border-radius: 4px;
            font-family: Roboto, sans-serif;
            font-size: 14px;
            height: 36px;
            cursor: pointer;
            padding: 0 24px
        }
        
        .dismissButton:hover {
            background-color: rgba(66, 133, 244, 0.04);
            border: 1px solid #d2e3fc
        }
        
        .dismissButton:focus {
            background-color: rgba(66, 133, 244, 0.12);
            border: 1px solid #d2e3fc;
            outline: 0
        }
        
        .dismissButton:hover:focus {
            background-color: rgba(66, 133, 244, 0.16);
            border: 1px solid #d2e2fd
        }
        
        .dismissButton:active {
            background-color: rgba(66, 133, 244, 0.16);
            border: 1px solid #d2e2fd;
            box-shadow: 0 1px 2px 0 rgba(60, 64, 67, 0.3), 0 1px 3px 1px rgba(60, 64, 67, 0.15)
        }
        
        .dismissButton:disabled {
            background-color: #fff;
            border: 1px solid #f1f3f4;
            color: #3c4043
        }
    </style>
    <style>
        .gm-style .gm-style-mtc label,
        .gm-style .gm-style-mtc div {
            font-weight: 400
        }
    </style>
    <style>
        .gm-control-active>img {
            box-sizing: content-box;
            display: none;
            left: 50%;
            pointer-events: none;
            position: absolute;
            top: 50%;
            transform: translate(-50%, -50%)
        }
        
        .gm-control-active>img:nth-child(1) {
            display: block
        }
        
        .gm-control-active:hover>img:nth-child(1),
        .gm-control-active:active>img:nth-child(1) {
            display: none
        }
        
        .gm-control-active:hover>img:nth-child(2),
        .gm-control-active:active>img:nth-child(3) {
            display: block
        }
    </style>
    <style>
        .gm-style .gm-style-cc span,
        .gm-style .gm-style-cc a,
        .gm-style .gm-style-mtc div {
            font-size: 10px;
            box-sizing: border-box
        }
    </style>
    <style>
        @media print {
            .gm-style .gmnoprint,
            .gmnoprint {
                display: none
            }
        }
        
        @media screen {
            .gm-style .gmnoscreen,
            .gmnoscreen {
                display: none
            }
        }
    </style>
    <style>
        .gm-style-pbc {
            transition: opacity ease-in-out;
            background-color: rgba(0, 0, 0, 0.45);
            text-align: center
        }
        
        .gm-style-pbt {
            font-size: 22px;
            color: white;
            font-family: Roboto, Arial, sans-serif;
            position: relative;
            margin: 0;
            top: 50%;
            -webkit-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            transform: translateY(-50%)
        }
    </style>
    <style>
        .gm-style img {
            max-width: none;
        }
        
        .gm-style {
            font: 400 11px Roboto, Arial, sans-serif;
            text-decoration: none;
        }
    </style>
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/slick.css">

    <meta name="description" content="When you engage Hunt &amp; Hunt Lawyers, you access a vast pool of resourceful lawyers who are passionate about the law and providing high-quality advice">
    <meta name="keywords" content="Hunt &amp; Hunt Lawyers, who we are, about us, our people, what we do, services, sectors, government, not-for-profit, insurance, business, private clients, national firm, locations, join us, aged care, agribusiness, alpine, automotive, banking and finance, building and construction, property, customs, china advisory ">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1"> -->
    <!-- <script type="text/javascript" charset="UTF-8" src="js/common.js"></script>
    <script type="text/javascript" charset="UTF-8" src="js/util.js"></script>
    <script type="text/javascript" charset="UTF-8" src="js/map.js"></script>
    <script type="text/javascript" charset="UTF-8" src="js/marker.js"></script>
    <script type="text/javascript" charset="UTF-8" src="js/stats.js"></script>
    <script type="text/javascript" charset="UTF-8" src="js/onion.js"></script>
    <script type="text/javascript" charset="UTF-8" src="js/controls.js"></script>
    <script type="text/javascript" charset="UTF-8" src="js/slick.js"></script> -->
    <!--=== TITLE ===-->  
    <!-- <title><?php wp_title(); ?> - <?php bloginfo( 'name' ); ?></title> -->
    <title><?php wp_title(''); ?></title>
    <!--=== WP_HEAD() ===-->
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <?php wp_body_open(); ?>

    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <header class="home-header" style="background: url('<?php echo get_template_directory_uri(); ?>/inc/img/divorce-separation.jpg') center top / 100% no-repeat;">
            <?php //include "block/header.php" ?>
            <?php get_template_part( 'template-parts/block/header'); ?>
            <?php //include "block/home-carousel.php" ?>
            <?php get_template_part( 'template-parts/block/home-carousel'); ?>
            
        </header>