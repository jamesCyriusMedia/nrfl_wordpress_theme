<?php get_header('home'); ?>
<section class="preferred-office panel">
    <?php // include "block/home-office.php" ?>
    <?php get_template_part( 'template-parts/block/home-office'); ?>
</section>

<section class="services">
    <?php //include "block/home-services.php" ?>
    <?php get_template_part( 'template-parts/block/home-services'); ?>
</section>

<!-- <section class="transition-picture" style="background-image: url('<?php echo get_template_directory_uri(); ?>/inc/img/home-banner.jpg');">
    <?php //include "block/home-transition.php" ?>
    <?php //get_template_part( 'template-parts/block/home-transition'); ?>
</section> -->

<!--<section class="news">
    <?php // include "block/home-news.php" ?>
</section>-->

<section class="our-locations">
    <?php //include "block/home-locations.php" ?>
    <?php get_template_part( 'template-parts/block/home-locations'); ?>
</section>
<?php get_footer(); ?>