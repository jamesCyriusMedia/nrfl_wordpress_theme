<?php get_header(); ?>
   <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <header style="background: url('<?php echo get_the_post_thumbnail_url(get_option('page_for_posts', true)) ?>') center no-repeat; background-size: cover; padding-top: 250px;">
        <?php //include "block/header.php" ?>
        <?php get_template_part( 'template-parts/block/header'); ?>
</section>
    </header>

    <section class="preferred-office panel">
        <div class="container">
            <h1 class="red-bar-heading"><?php echo get_the_title( get_option('page_for_posts', true) ); ?></h1>
        </div>
    </section>
    <section class="main-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <?php //include $block; ?>
                    <!-- Content -->
                    <!-- query -->
                    <?php
                        $args  = array(
                            'post_type'      => 'post',
                            'post_status'    => 'publish',
                            'posts_per_page' => -1
                        );
                        $query = new WP_Query( $args );
                    ?>

                    <?php if ( $query->have_posts() ) : ?>

                    <!-- begin loop -->
                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>

                        <h3><a href="<?php the_permalink(); ?>" title="Read"><?php the_title(); ?></a></h3>
                        <?php the_excerpt(); ?>
                        <?php echo get_the_date(); ?>

                    <?php endwhile; ?>
                    <!-- end loop -->


                    <div class="pagination">
                        <?php 
                            echo paginate_links( array(
                                'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                                'total'        => $query->max_num_pages,
                                'current'      => max( 1, get_query_var( 'paged' ) ),
                                'format'       => '?paged=%#%',
                                'show_all'     => false,
                                'type'         => 'plain',
                                'end_size'     => 2,
                                'mid_size'     => 1,
                                'prev_next'    => true,
                                'prev_text'    => sprintf( '<i></i> %1$s', __( 'Newer Posts', 'text-domain' ) ),
                                'next_text'    => sprintf( '%1$s <i></i>', __( 'Older Posts', 'text-domain' ) ),
                                'add_args'     => false,
                                'add_fragment' => '',
                            ) );
                        ?>
                    </div>


                    <?php wp_reset_postdata(); ?>

                    <?php else : ?>
                        <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                    <?php endif; ?>
                </div>
                <div class="col-sm-4">
                    <?php //include $menu; ?>
                    <!-- Sidebar -->
                    <?php get_template_part( 'template-parts/block/resources-menu'); ?>
                    <?php get_template_part( 'template-parts/block/resource-blog-menu'); ?>
                    <?php get_template_part( 'template-parts/block/sidebar-location-contact'); ?>
                </div>
            </div>
        </div>
    </section>
    <?php get_footer(); ?>