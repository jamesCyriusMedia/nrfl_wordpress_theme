<?php get_header(); ?>
   <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]--> 
    <?php $thumbnail = (get_the_post_thumbnail_url() == "" ? get_template_directory_uri() . "/inc/img/inner-header-1.jpg" : get_the_post_thumbnail_url() ); ?>
    <header style="background: url('<?php echo $thumbnail ?>') center no-repeat; background-size: cover; padding-top: 250px;">
        <?php //include "block/header.php" ?>
        <?php get_template_part( 'template-parts/block/header'); ?>
    </header>

    <section class="preferred-office panel">
        <div class="container">
            <h1 class="red-bar-heading">404 Error</h1>
        </div>
    </section>
    <section class="main-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php //include $block; ?>
                    <!-- Content -->
					Sorry we couldnt find the page you were requesting
                </div>
                <!-- <div class="col-sm-4"> -->
                    <?php //include $menu; ?>
                    <!-- Sidebar -->
                    <?php //get_template_part( 'template-parts/block/about-menu'); ?>
                    <?php //get_template_part( 'template-parts/block/resources-menu'); ?>
                    <?php //get_template_part( 'template-parts/block/resource-blog-menu'); ?>
                    <?php //get_template_part( 'template-parts/block/sidebar-location-contact'); ?>
                </div>
            </div>
        </div>
    </section>
    <?php get_footer(); ?>