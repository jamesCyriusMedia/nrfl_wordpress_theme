

    <p class="bigger-red-text">Hunt &amp; Hunt is a national firm with offices in Sydney (CBD and North Ryde), Melbourne, Brisbane, Perth, Darwin and Shanghai.</p>
    <p>Hunt &amp; Hunt is also a member of <a href="http://interlaw.org/" target="_blank" title="Interlaw">Interlaw</a>, an international network of quality-monitored, corporate, commercial and independent law firms. </p>
    <br>
    <h4>NORTH RYDE</h4>

    <p>Hunt &amp; Hunt's North Ryde office is conveniently located on Herring Road (cnr Innovation Road) opposite the Macquarie Shopping Centre. We assist large and small businesses, as well as private clients in Sydney's north west. As a member of the local community for over 40 years, we know the north west region and appreciate the needs of local businesses, individuals and industry. Our clients benefit from this local knowledge, as well as the on-the-ground support we are able to provide.</p>
    <br>
    <h4>Business</h4>
    <p>We work extensively with businesses across all areas of law including property, commercial and employment law incorporating contracts, franchises, sales of businesses and commercial retail leasing. We work closely with our clients to provide representation, advice and documentation and assist with negotiations and managing disputes.</p>
    <br>
    <h4>Individual</h4>
    <p>We are here to help through life's ups and downs - whether you are buying a home, going through a separation or starting a business. We provide a range of advice to individuals including property and conveyancing, family law, wills, estate planning and probate. </p>
    <br>
    <h4>Family Law </h4>
    <p>Our family law specialist can guide you through issues arising from your relationship or marriage.</p>

    <p>Services include child support and custody, financial and property matters.</p>
    <br>
    <h4>Not-for-profit</h4>
    <p>To help our not-for-profit clients minimise risk and remain compliant with relevant laws, we provide advice on commercial law including contracts and agreements; corporate governance; employment and workplace relations; property and leasing; charitable trusts; bequest clauses in wills and intellectual property and asset protection strategies. We also advise on company and charitable structures, and can help not-for-profit organisations choose the structure that best suits their activities, while accommodating the varied needs of relevant interest groups.</p>

    <p>Hunt &amp; Hunt North Ryde is proactive in the local business community, contributing to charities and operating a formal pro-bono program. We proudly support local organisations including:</p>

    <p>Anglicare | Arden Anglican School | Barker College | Centre for Disability Studies | Forsight Foundation for the Deaf and Blind | Friends and neighbours Inc | Hamlin Fistula Ethiopia | Hammond Care Group | Life After Prison | Macquarie Community College | St Andrews Cathedral School | Sydney Anglican Church | Uniting Care Burnside | Uniting Church.</p>
    <br>
    <p><a class="btn  button-default" href="/contact" role="button">CONTACT US</a></p>

