
    <p class="black-title">Insurance is a competitive, complex and emotionally charged area of law. For more than 80 years, Hunt &amp; Hunt's insurance law service has provided regulatory advice, claims assessments and litigation services, achieving outstanding results for its clients along the way.</p>
    <p>Hunt &amp; Hunt's insurance lawyers work in every Australian capital city, providing localised, timely advice to ensure clients meet their personal and business goals.
    </p>
    <p>We'll help you minimise costs by ensuring the right lawyers – with the appropriate expertise and experience – manage your work. We'll identify the crucial issues and deliver a tailored and practical solution in our trademark approachable manner.
    </p>

    <br>
    <h4>WHO WE ACT FOR</h4>
    <p>Our national team of insurance lawyers acts for Australian and overseas insurers, public sector risk carriers, brokers and reinsurers, including:</p>
    <ul>
        <li>Association and mutual insurers</li>
        <li>Captive insurers</li>
        <li>Compulsory third-party insurers</li>
        <li>Contract works liability and material damage insurers</li>
        <li>Corporate clients</li>
        <li>Local councils</li>
        <li>Professional indemnity insurers</li>
        <li>Public and products liability insurers</li>
        <li>Reinsurers</li>
        <li>State government departments</li>
        <li>Underwriting agencies</li>
        <li>Workers compensation insurers.</li>
    </ul>
    <br>
    <h4>COMPULSORY THIRD-PARTY INSURANCE</h4>
    <p>Hunt &amp; Hunt is a leading defender of compulsory third-party claims. We offer our clients a great depth of knowledge and experience across the full spectrum of claims, from standard through to catastrophic, fraud and complex claims. Find out more.
    </p>

    <br>
    <h4>DUST DISEASES INSURANCE</h4>
    <p>Our insurance lawyers provide specialist advice and litigation services on cases involving dust diseases. We defend the interests of insured and uninsured corporate entities against dust diseases claims. Find out more.
    </p>

    <br>
    <h4>WORKERS COMPENSATION</h4>
    <p>Our expert insurance lawyers have defended self-insured individuals, small and large employers, and government agencies against workers compensation claims. We can also help employers develop strategies to reduce the incidence of claims. Find out more
    </p>

    <br>
    <h4>COMMERCIAL AND CASUALTY INSURANCE</h4>
    <p>Do you have a personal injury or property damage claim? We provide advice to reinsurers, insurers, agents and public authorities, defending insurers and the insured in courts across Australia and internationally. Hunt &amp; Hunt's depth of resources and insurance experience means we can take on matters of any size – from small personal and domestic claims to complex engineering disputes, coverage disputes and class actions. Find out more
    </p>

    <br>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                              		Commercial and casualty	

                            </a>
                          </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">

                    <p>Personal injury and property damage claims can be expensive and difficult to resolve. At Hunt &amp; Hunt, we can help cost-effectively finalise claims and implement risk management systems to help prevent this type of claim arising.
                    </p>
                    <p>Hunt &amp; Hunt works with insurers and other organisations on public liability, product liability, contract works liability, and material damage and professional indemnity issues.
                    </p>
                    <p>Our lawyers provide expert advice to help clients comply with relevant regulations, implement suitable coverage, draft indemnities, evaluate the adequacy of existing insurance programs and undertake legal risk audits. We also help draft insurance and reinsurance policies, help clients lodge claims, inform reinsurers about pollution exclusions, and advise on medical and dental negligence law.
                    </p>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                              		Compliance and regulation	

                            </a>
                          </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">

                    <p>Hunt &amp; Hunt advises general insurers on claims work, regulatory obligations and the impact of changes to legislation.</p>
                    <p>We meet the complex needs of general insurers by building a deep understanding of their businesses, and by establishing close relationships with the government agencies and industry bodies that regulate the insurance sector.
                    </p>
                    <p>In recent years, our insurance specialists have advised clients on Australian Prudential Regulation Authority (APRA) capital requirements, Australian Securities and Investments Commission (ASIC) licensing requirements and the general insurance code of practice.
                    </p>
                    <p>Hunt &amp; Hunt has established a strong reputation within the public sector for the quality of its legal services and has worked with the Federal Government to clarify proposals affecting the insurance industry. For example, in the 2009/2010 financial year, we worked closely with ASIC and the Financial Ombudsman Service to draft new terms of reference and accompanying operational guidelines to resolve general insurance disputes. In doing so, we helped insurers gain concessions and relief from potentially onerous and costly proposed provisions.
                    </p>
                    <p>This involvement in government work allows us to ensure our clients are fully aware of and can prepare for pending legislative and regulatory changes.
                    </p>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
                <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                              	Compulsory third party	

                            </a>
                          </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">

                    <p>Hunt &amp; Hunt's longstanding insurance practice is one of Australia's finest. Our lawyers are experts in defending compulsory third party claims and act for some of Australia's largest compulsory third party insurers.
                    </p>
                    <p>Hunt &amp; Hunt's compulsory third party (CTP) specialists have significant experience advising businesses on CTP insurance claims. We are proactive and dedicated, and always transparent in our dealings with clients.
                    </p>
                    <p>Our work includes standard, complex, major, catastrophic, infant and fraud claims, as well as test cases. We also advise on insurance liability and causation issues, compensation to relative claims, recovery actions and indemnity issues.
                    </p>
                    <p>To help our clients manage costs while fostering cooperation and resolution, our insurance lawyers have devised a range of best practice CTP legal and business strategies.
                    </p>

                    <h4>Leaders in compulsory third party insurance</h4>
                    <p>Within our national insurance practice, our CTP lawyers have a deep knowledge of the Australian insurance industry, and stay up to date on all issues and development relating to CTP.
                    </p>
                    <p>In South Australia we regularly produce a free Compulsory Third Party Handbook, which summarises all relevant legislation, scales and multipliers, as well as information necessary for the fast handling of claims. We provide this highly sought-after guide to our clients and to opposing lawyers. We have been praised by the CTP industry for the spirit of cooperation this handbook inspires.
                    </p>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingFour">
                <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                              		Dust diseases	

                            </a>
                          </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingFour">
                <div class="panel-body">

                    <p>With more than 20 years of experience defending clients in dust diseases litigation, Hunt &amp; Hunt can confidently guide businesses through this complex and often contentious area.</p>
                    <p>Hunt &amp; Hunt's specialists in dust diseases litigation and environmental contamination deliver high-quality and timely legal services to companies and government organisations across Australia. </p>

                    <p>Our team has advised clients on dust diseases litigation and asbestos regulations since the 1980s. Our lawyers have an in-depth understanding of dust issues and related diseases. We are also familiar with the different types of asbestos suppliers and manufacturers worldwide. This means we have already done much of the groundwork and can readily draw on this knowledge to act quickly on claims.
                    </p>

                    <h4>Our experts help protect your business</h4>
                    <p>Hunt &amp; Hunt has defended many insured and uninsured corporate entities against claims for damages from asbestos and silica exposure. These claims have been made by employees at workplaces including building sites, cement works, dockyards, factories, mines, oil refineries, power stations, quarries and ships.
                    </p>
                    <p>Asbestos litigation has driven many companies around the world into bankruptcy. To help protect your business, we can advise on local laws and procedures relating to asbestos and silica exposure. Our lawyers can also advise on managing asbestos in properties and asbestos spills. </p>
                </div>
                <p></p>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingFive">
                <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                              		Workers compensation	

                            </a>
                          </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingFive">
                <div class="panel-body">

                    <p> Hunt &amp; Hunt has provided workers compensation services for more than 70 years. We are one of few Australian law firms to offer such strong experience and expertise in this niche area of law.</p>
                    <p>Our workers compensation lawyers are experts in all aspects of claims management and litigation. We have helped numerous private sector and government clients avoid unfair dismissal and discrimination claims. </p>

                    <p>Hunt &amp; Hunt has defended claims for every type of industrial matter, including those involving lump sums; all types of claims for weekly payments – both in the Workers Compensation Commission and at common law; death claims; medical expenses; and domestic assistance.
                    </p>
                    <p>To help protect your business, Hunt &amp; Hunt lawyers provide advice and assistance that goes beyond claim handling. We have a deep involvement in the insurance industry. Our in-depth understanding of workers compensation legislation and related issues means we can provide you with clear and comprehensive legal advice. </p>
                </div>
                <p></p>
            </div>
        </div>

    </div>

