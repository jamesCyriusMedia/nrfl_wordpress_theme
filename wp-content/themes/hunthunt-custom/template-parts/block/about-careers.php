
    <p class="bigger-red-text">Our people make Hunt &amp; Hunt an invigorating and positive place to work.</p>
    <br>
    <h4>WELCOME TO HUNT &amp; HUNT</h4>
    <p> We place a strong focus on the professional development of our legal and internal support professionals. Our staff retention rates are the envy of other firms and are a direct result of our commitment to long-term career development and a healthy work/life balance. We believe achieving a balance between work and family life is incredibly important. </p>
    <br>
    <h4>TALENT DEVELOPMENT</h4>
    <p>Our human resources and business development teams work closely to nurture talent. This means our lawyers and support staff are thoroughly prepared before taking their next steps up the career ladder. </p>
    <p>We recognise the importance of strong mentors and our mid-tier structure means our partners are hands-on and readily accessible in all matters. </p>
    <p> We place a strong emphasis on ongoing training and development, and offer our employees extensive technical and professional training, presentations by thought leaders in relevant industries and CLE seminars. </p>
    <p>Our generous study support policies include leave for staff who undertake postgraduate study. Many of our lawyers have completed a masters degree in law and other relevant disciplines while working at Hunt &amp; Hunt. </p>
    <br>
    <h4>EQUAL OPPORTUNITIES</h4>
    <p>Hunt &amp; Hunt's equal opportunity policy ensures all employees have equal access to employment and its benefits. We treat all staff equally and select our people on the basis of their skills, qualifications, abilities, aptitude and performance. </p>
    <p> The Equal Opportunity for Women in the Workplace Agency (EOWA) awarded Hunt &amp; Hunt 'waived status' under the Equal Opportunity for Women in the Workplace Act 1999 for our high level of workplace program analysis, consultation with staff and formal action to address issues. EOWA commended Hunt &amp; Hunt for its training and development program and the firm's promotion of women to partner and associate levels. </p>
    <p> For more information or to apply for a position with Hunt &amp; Hunt, email <a href="mailto:human_resources@huntnsw.com.au" title="email us">human_resources@huntnsw.com.au</a> </p>
