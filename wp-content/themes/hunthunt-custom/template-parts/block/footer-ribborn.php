<div class="sub-footer">
	<div class="container">
		<p>© <span id="current-year"><?php echo date("Y"); ?></span> Hunt &amp; Hunt Lawyers | <a href="/terms-and-conditions/" title="Terms & Conditions">Terms & Conditions</a> | Site by <a href="http://www.cyrius.com.au/" target="_blank" title="Cyrius Media Group">Cyrius</a></p>
	</div>
</div>