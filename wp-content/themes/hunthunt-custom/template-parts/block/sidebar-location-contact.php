<aside style="margin-top: 40px;">
    <div>
        <?php $parenttitle = "Location / Contact" ?>
        <h4 class="black-title"><?php echo $parenttitle ?></h4>
    </div>
    <div style="margin: 0 0 30px 50px;">
        <b>Do you have a family law or private legal question?</b><br><br>
        <p>
            <strong>Contact us today.</strong><br>
            <span class="fa fa-phone"></span> Phone: <a href="tel:0298045700">02 9804 5700</a> or<br>
            <span class="fa fa-envelope"></span> Email: <a href="mailto:info@huntnsw.com.au?subject=North Ryde Website Legal Enquiry">info@huntnsw.com.au</a>
        </p>
        <p>Or arrange to visit us at our North Ryde office, directly opposite Macquarie Centre (click map for more).</p>
        <br>
        <a href="/contact-us"><img alt="" src="<?php echo get_template_directory_uri(); ?>/inc/img/sidebar-map.jpg"></a>
    </div>
</aside>