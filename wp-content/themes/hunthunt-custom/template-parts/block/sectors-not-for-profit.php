
    <p class="bigger-red-text">Hunt &amp; Hunt understands the specialised needs of this diverse sector and we're passionate about the work we do.</p>

    <p>Hunt &amp; Hunt has supported not-for-profit clients for more than 80 years. We appreciate the changing legal environment in which these organisations operate and we stay up to date on all relevant issues.
    </p>
    <p>The experienced lawyers in our not-for-profit practice work with clients in the aged care industry, charities, churches, clubs, foundations, hospitals, industry groups, member-based organisations and educational organisations. They take the time to understand clients' needs, and provide tailored, specialist legal advice in a friendly and straightforward way.
    </p>
    <br>
    <h4>PERSONALISED, HOLISTIC LEGAL SERVICES FOR NOT-FOR-PROFIT ORGANISATIONS</h4>
    <p>We pride ourselves on providing the commercial and legislative advice our not-for-profit clients need to achieve their business goals.
    </p>
    <p>To help our not-for-profit clients minimise risk and remain compliant with relevant laws, our specialists provide advice on commercial law, including contracts and agreements; corporate governance; employment and workplace relations; and intellectual property and protection strategies.
    </p>
    <p>We also advise on company structures and can help not-for-profit organisations choose the structure that best suits their activities, while accommodating the varied needs of relevant interest groups.
    </p>
    <br>
    <h4>FOCUSED ON TRANSPARENCY AND ACCOUNTABILITY</h4>
    <p>For not-for-profit organisations, and charities in particular, transparency and accountability are essential. With this in mind, we provide legal advice on developing and running fundraising campaigns, complying with complex legislation and exceeding the minimum operational standards set by law.
    </p>
    <p>We also offer expert advice on:</p>
    <ul>
        <li>accessing relevant tax concessions</li>
        <li>applying for, obtaining and maintaining endorsements and registrations</li>
        <li>charitable trusts and bequest clauses in wills</li>
        <li>litigation and dispute resolution</li>
        <li>property and leasing.</li>
    </ul>

