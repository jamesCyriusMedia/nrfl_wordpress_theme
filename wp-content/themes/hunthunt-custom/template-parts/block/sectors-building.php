

    <p class="bigger-red-text">Hunt &amp; Hunt specialises in building and construction law and works with construction companies, sub-contractors, engineers, architects, planners and financiers to guide client projects from early planning to post-build stages.</p>

    <p>Our building and construction lawyers negotiate and document contracts on behalf of engineering firms, builders and their customers in the areas of construction, infrastructure and mining. These contracts cover Australian standard construction, project management and maintenance agreements. We also advise on project management disputes and security for payment claims.
    </p>
    <p>Our building and construction law experts have many years of experience advising on residential, commercial and industrial projects across the country, and have developed a deep understanding of local laws and regulations. This allows us to advise on governance, contract and enforcement requirements affecting building and construction projects in a range of jurisdictions.
    </p>
    <p>We are experts in:</p>
    <ul>
        <li> contract drafting, advice and enforcement; including standard form and special purpose agreements</li>
        <li>tender preparation and advice</li>
        <li>drafting joint venture, alliance and management agreements</li>
        <li>advising on risk management and distribution</li>
        <li>company prospectuses, public disclosure statements and project finance</li>
        <li>advising on existing and planned legislation.</li>
    </ul>
    <p> We can also advise on construction security for payment claims, development applications, professional liability and negligence, insurance claims and resolution, and debt recovery.</p>
    <br>
    <h4>DISPUTE RESOLUTION AND REPRESENTATION</h4>
    <p>Our team can help resolve disputes quickly and commercially, or advise on the conduct of and representation at mediations. In addition, we have experience in traditional form litigation, as well as arbitration and adjudication. We have also provided representation in cases where clients have been prosecuted before the Building Practitioners Board, Building Appeals Board or other relevant industry authorities.
    </p>
    <br>
    <p class="literature-container"> <span class="icon"><img src="/img/pdf.png" alt="Building and Construction" style="width:16px;height:16px;border:0px;"></span> <span class="name"><a href="/pdf/BuildingandConstruction.pdf">Building and Construction</a></span> (735 KB) </p>

    <br>

    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                              	Domestic building disputes
                            </a>
                          </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">

                    <p>Hunt &amp; Hunt are experts in domestic building law, construction contracts and work with construction companies, builders, architects, owners and owner's corporations across a full spectrum of building and construction issues.
                    </p>
                    <p> We negotiate and document building contracts and construction contracts, and advise on project management and building disputes. Our clients benefit from the resources of a national law firm, whilst having direct access to lawyers with an in depth understanding of local domestic building law.
                    </p>
                    <p>Our main priority is to offer efficient, pragmatic and commercially astute advice to simplify the onerous and often complex pathways that you may face when commencing a project or when a domestic building dispute arises.
                    </p>
                    <p>We regularly act for local governments, businesses and private clients. This means that we are aware of the full range of issues involved in domestic building disputes and understand the needs and requirements of all parties.
                    </p>
                    <p>We are adept at understanding and interpreting local laws and regulations and advise clients in relation to drafting and administering building contracts and representing clients in relation to any type of dispute.</p>
                    <p>Our building lawyers are experts in:</p>
                    <ul>
                        <li>Acting for Owners Corporations in domestic building disputes
                        </li>
                        <li>Acting for owners and builders in domestic building disputes
                        </li>
                        <li>Providing Protection Works advice
                        </li>
                        <li>Conducting litigation in all jurisdictions
                        </li>
                        <li>Appearing and advocating at mediations and conciliation conferences
                        </li>
                        <li>Briefing experts in relation to obtaining detailed defect reports
                        </li>
                        <li>Drafting and reviewing building contracts and special conditions
                        </li>
                        <li>Drafting joint venture and management agreements
                        </li>
                        <li>Providing advice on sample building contract and design and construct contracts
                        </li>
                        <li>Advising on risk management and distribution
                        </li>
                        <li>Acting on behalf of builders in relation to Building
                        </li>
                        <li>Prosecutions and Building Appeals Board matters
                        </li>
                        <li>Provide advice on dealing with disputes in construction.</li>
                    </ul>
                    <p>Should you require advice in relation to a domestic building project or dispute, please do not hesitate to contact one of our expert lawyers.</p>

                </div>
            </div>
        </div>
    </div>
