<nav class="navbar navbar-fixed-top affix-top" data-spy="affix" data-offset-top="100">
<div>
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="<?php echo get_template_directory_uri(); ?>/inc/img/hunt-and-hunt-logo.png" alt="Hunt&amp;Hunt logo" data-spy="affix-top"></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="/">Home <span class="sr-only">(current)</span></a></li>
                        <li class="dropdown">
                            <a href="/about-us" title="About Us" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About Us</a>
                            <ul class="dropdown-menu">
                                <li><a href="/about-us" title="About Hunt & Hunt North Ryde">About Hunt & Hunt North Ryde</a></li>
                                <li><a href="/about-us/our-people" title="Our People">Our People</a></li>
                                <li><a href="/about-us/membership-association" title="Membership & Associations">Membership & Associations</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="/services" title="Services" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services</a>
                            <ul class="dropdown-menu">
                                <li><a href="/services" title="Services Overview">Services Overview</a></li>
                                <li><a href="/services/divorce-and-separation" title="Divorce and Separation">Divorce and Separation</a></li>
                                <li><a href="/services/property-settlement" title="Property Settlement">Property Settlement</a></li>
                                <li><a href="/services/spouse-maintenance-and-child-support" title="Spouse Maintenance & Child Support">Spouse Maintenance & Child Support</a></li>
                                <li><a href="/services/parenting" title="Parenting">Parenting</a></li>
                                <li><a href="/services/financial-agreements" title="Financial Agreements">Financial Agreements</a></li>
                                <li><a href="/services/wills-and-estate-planning" title="Wills and Estate Planning">Wills & Estate Planning</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="/blog" title="Resources" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Resources</a>
                            <ul class="dropdown-menu">
                                <li><a href="/resources" title="News & Articles">News & Articles</a></li>
                                <li><a href="/resources/faqs" title="FAQs">FAQs</a></li>
                                <li><a href="/resources/links" title="Links">Links</a></li>
                            </ul>
                        </li>
                        <li class=""><a href="/contact-us">Contact Us</a></li>
                    </ul>
                </div>
                <!--/.navbar-collapse -->
            </div>
        </nav>