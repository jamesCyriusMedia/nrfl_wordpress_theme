

    <p class="bigger-red-text">Australia's alpine industry has a unique set of challenges and opportunities. Our lawyers have a thorough understanding of the issues facing this sector, on and off the snowfields.</p>
    <p>Our highly specialised alpine industry practice has extensive experience providing advice to clients including the ski lift and snow-sports school operators at two of Victoria's leading alpine resorts. We also advise alpine associations, property developers, business and tourism operators, resort management boards and providers of alpine accommodation facilities.</p>

    <p>Our lawyers advise clients on all facets of competition and consumer laws related to tourism developments. Many of these matters are unique to the alpine environment. We also advise clients in this sector on employee and severance-related agreements and benefit plans.</p>
    <br>
    <h4>ALPINE DEVELOPMENT AND CONSTRUCTION</h4>
    <p>We regularly advise clients on matters involving site acquisition, financing and development, project design and construction, all of which involve issues unique to alpine resorts. </p>

    <p>Our lawyers also advise on related matters such as environmental impact assessments and concerns, flora and fauna protection, Crown Land leaseholds, Indigenous land rights issues, risk management plans and risk assessments.</p>
    <br>
    <h4>WINTER AND SUMMER ALPINE SPORTS ACTIVITIES</h4>
    <p>Our lawyers are familiar with the unique challenges and high-risk environments in which winter and summer sports and training activities are conducted. </p>

    <p>We regularly provide advice on risk mitigation, media, advertising, concessions, merchandising, and race and sponsorship arrangements.</p>
    <br>
    <h4>INSURANCE LITIGATION SERVICES FOR ALPINE ACTIVITIES</h4>
    <p>Our comprehensive litigation services cover all facets of alpine activities, including:</p>
    <ul>

        <li> collisions – where skiers and/or snowboarders collide and one skier and/or snowboarder sues the other (including ski/board racing events)</li>
        <li>falls – where skiers, snowboarders, cyclists, trail bike users, bushwalkers, campers and the like have an accident while on a mountain, and the </li>
        <li>fault of the injury lies with an improperly designed, maintained, marked or groomed slope, trail or activity facility</li>
        <li>lift operation and maintenance – where a skier or snowboarder is injured as a result of a faulty, defective or improperly maintained lift or any equipment used in the alpine environment</li>
        <li>instructor injuries – where a skier or snowboarder, under the supervision of an instructor, is led into unmanageable terrain and injures themselves, or where a skier or snowboarder, under the supervision of an instructor, injures a third party and that injury is a result of inadequate supervision and/or instruction</li>
        <li>premises liability – including trips and slips in or around buildings</li>
        <li> general occupier's liability</li>
        <li> snowmaking and snowmobile operation</li>
    </ul>
    <br>
    <p class="literature-container"> <span class="icon"><img src="/img/pdf.png" alt="Alpine capability" style="width:16px;height:16px;border:0px;"></span> <span class="name"><a href="/pdf/Alpine.pdf">Alpine capability</a></span> (3453 KB) </p>
