
    <p class="bigger-red-text">Hunt &amp; Hunt's national M&amp;A team has a substantial array of expertise and depth of experience in advising clients on mergers, acquisitions, disposals and joint ventures – both in Australia and internationally. We are focused on assisting clients to seize opportunities and achieve their commercial objectives quickly, efficiently and with minimal exposure to risk.</p>

    <br>
    <h4>EXTENSIVE EXPERIENCE IN M&amp;A</h4>
    <p>Hunt &amp; Hunt has recently advised on many transactions, ranging in value from $5m to in excess of $100m. We have assisted a diverse range of clients in acquisitions, divestments and joint ventures, including large international and Australian corporates, Government-owned entities, private equity funds and private business owners. We have particular expertise working with private business owners to build and add to their business and also plan an exit transaction that maximises value.</p>

    <p>We have also been appointed to the Due Diligence Committee for transactions.</p>

    <p>Our depth of commercial understanding of our clients' businesses contributes significantly to an exceptional outcome. Due Diligence is an area where many firms carry out far too much unnecessary work. We discuss with the client an appropriate scope of works for this elastic issue.</p>

    <br>
    <h4>CAPTURING THE DEAL SWIFTLY</h4>
    <p>Underpinned by robust legal analysis, our focus is to work with our clients and their advisors to prioritise the crucial issues and deliver the right solutions quickly, whilst still minimising risk and business disruption. Identifying the highest priorities immediately and actioning them first is vital to the client achieving a successful transaction. Several of our team are directors of companies, so we see the commercial realities immediately.</p>

    <br>
    <h4>REAL CHINESE EXPERIENCE</h4>
    <p>Our M&amp;A team has genuine experience in advising Chinese clients over almost 20 years and is supported by our own Shanghai office. Since 1998, we have been and remain the only mid-tier Australian law firm licensed to operate in China.</p>

    <br>
    <h4>COMPLEMENTARY SKILLSETS</h4>
    <p>We also offer the benefit of experts in the areas often ancillary to but crucial for a successful M&amp;A transaction (for example, employment, property, including environmental and leasing, regulatory compliance, intellectual property and information technology), and we ensure that all our expertise is used strategically, and is outcome-focused.</p>

    <br>
    <h4>INTERLAW</h4>
    <p>We are the only Australian member of a joint venture of around 65 leading firms in most countries of the world where business is transacted (covering 120 cities). This means that on a transaction with any international aspect, we can obtain quick advice which is worldwide in scope, yet local in expertise, culture and connections.</p>
    <p class="literature-container"> <span class="icon"><img src="/img/pdf.png" alt="M&amp;A Capability" style="width:16px;height:16px;border:0px;"></span> <span class="name"><a href="/pdf/MA.pdf">M&amp;A Capability</a></span> (1157 KB) </p>
    <p class="literature-container"> <span class="icon"><img src="/img/pdf.png" alt="Business Exits" style="width:16px;height:16px;border:0px;"></span> <span class="name"><a href="/pdf/BusinessExits.pdf">Business Exits</a></span> (280 KB) </p>
    <p class="literature-container"> <span class="icon"><img src="/img/pdf.png" alt="Can a family constitution help your family business thrive?" style="width:16px;height:16px;border:0px;"></span> <span class="name"><a href="/pdf/2015-02-11FamilyConstitution.pdf">Can a family constitution help your family business thrive?</a></span> (285 KB) </p>

