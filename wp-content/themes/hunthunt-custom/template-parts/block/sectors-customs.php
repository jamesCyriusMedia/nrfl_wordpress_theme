

    <p class="bigger-red-text">If your business is involved in customs and trade, let us help you take advantage of the opportunities, and manage the risks, associated with the changing global trade environment.</p>

    <p>The Australian customs and trade landscape has experienced considerable change with the introduction of new and significant free trade agreements (FTAs), the launch of the Australian Trusted Trader Program, a new regulatory authority(the Australian Border Force), and an anti-dumping regime that is becoming increasingly complex and difficult for traders. Our team can help guide your business through this changing market. We provide specialist advice on the complex domestic and international legal issues facing importers, exporters, customs brokers and freight forwarders in the customs and global trade sectors. We can assist you with:
    </p>
    <ul>
        <li>Customs compliance including representation during Australian Border Force audits, drafting and implementing compliance policies and managing voluntary disclosure – Learn More</li>
        <li>Free trade agreements - advising on, and assisting with, the utilisation of free trade agreements - Learn More</li>
        <li>Customs valuation and tariff classification including applying for rulings, internal review of decisions and Court and Tribunal review of decisions</li>
        <li>Customs &amp; transfer pricing – Learn More</li>
        <li>Reviewing use of tariff concession orders, duty drawback and other customs concessions</li>
        <li>Australian Trusted Trader Program applications– Learn More</li>
        <li>Export trade compliance – Learn More</li>
        <li>Strategic customs and supply chain planning</li>
        <li>Anti-dumping and countervailing duty investigations – Learn More</li>
        <li>Drafting agreements relating to international trade including supply, distribution and licence agreements</li>
        <li>Managing disputes between parties in the international supply chain.</li>
    </ul>
    <p>Our clients are varied and include some of the world’s largest multi-national companies to private businesses and individual customs brokers. No matter the business size or matter complexity, our clients benefit from our extensive experience in this specialised area.
    </p>
    <p class="literature-container"> <span class="icon"><img src="/img/pdf.png" alt="Customs and Global Trade" style="width:16px;height:16px;border:0px;"></span> <span class="name"><a href="/pdf/CGT.pdf">Customs and Global Trade</a></span> (771 KB) </p>

