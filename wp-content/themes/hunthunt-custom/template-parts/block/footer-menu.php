<div class="container">
            <nav>
                <div class="row">
                    <div class="col-sm-2">
                        <h4>MAIN LINKS</h4>
                        <ul>
                            <li><a href="/" title="Home Page">Home </a></li>
                            <li><a href="/about-us" title="Who we are">About Us </a></li>
                            <!--<li><a href="#" title="">Clients </a></li>-->
                            <li><a href="/services" title="Services">Services </a></li>
                            <!--<li><a href="#" title="">Community </a></li>-->
                            <!--<li><a href="/news" title="News and Publications">News &amp; Publications </a></li>
                            <li><a href="/events" title="Events">Events </a></li>-->
                        </ul>
                    </div>
                    <div class="col-sm-2">
                        <h4>Services</h4>
                        <ul>
                            <li><a href="/services" title="Services Overview">Services Overview</a></li>
                            <li><a href="/services/divorce-and-separation" title="Divorce and Separation">Divorce and Separation</a></li>
                            <li><a href="/services/property-settlement" title="Property Settlement">Property Settlement</a></li>
                            <li><a href="/services/spouse-maintenance-and-child-support" title="Spouse Maintenance and Child Support">Spouse Maintenance and Child Support</a></li>
                            <li><a href="/services/parenting/" title="Parenting">Parenting</a></li>
                            <li><a href="/services/financial-agreements" title="Financial Agreements">Financial Agreements</a></li>
                            <li><a href="/services/wills-and-estate-planning" title="Wills and Estate Planning">Wills and Estate Planning</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-2">
                        <h4>Resources</h4>
                        <ul>
                            <li><a href="/resources/" title="Resources Overview">Resources Overview</a></li>
                            <li><a href="/resources/faqs/" title="FAQs">FAQs</a></li>
                            <li><a href="/links/" title="Links">Links</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-2">
                        <h4>CONNECT WITH US</h4>
                        <a target="_blank" style="background:url('<?php echo get_template_directory_uri(); ?>/inc/img/facebook-logo-sprite.png') no-repeat;" href="https://www.facebook.com/hunthuntnorthryde" title="find us on Facebook" class="fb-button"></a>
                        <a target="_blank" style="background:url('<?php echo get_template_directory_uri(); ?>/inc/img/linkedin-logo-sprite.png') no-repeat;" href="https://www.linkedin.com/company/hunt-and-hunt-lawyers/" title="find us on Linkedin" class="in-button"></a>
                        <!-- <a style="background:url('<?php echo get_template_directory_uri(); ?>/inc/img/youtube-logo-sprite.png') no-repeat;" href="/about-us/who-we-are#" title="find us on Youtube" class="youtube-button"></a> -->
                    </div>
                    <div class="col-sm-4 prize-area">
                        <div><img src="<?php echo get_template_directory_uri(); ?>/inc/img/business-award-2014.png" alt="local business award 2014">
                            <p>WINNER 2014</p>
                            <p>Business of the Year</p>
                            Professional Services
                        </div>
                        <div><img src="<?php echo get_template_directory_uri(); ?>/inc/img/business-award-2015.png" alt="local business award 2015">
                            <p>FINALIST 2015</p>
                            <p>Professional Services</p>
                        </div>
                        <div><img src="<?php echo get_template_directory_uri(); ?>/inc/img/business-award-2016.png" alt="local business award 2016">
                            <p>WINNER 2016</p>
                            <p>Professional Services</p>
                        </div>
                    </div>
                </div>
            </nav>
        </div>