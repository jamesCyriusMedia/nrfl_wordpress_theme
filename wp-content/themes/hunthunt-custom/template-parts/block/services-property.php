
    <p class="bigger-red-text">Property transactions require careful, efficient negotiation and drafting. Hunt &amp; Hunt's property lawyers are skilled negotiators and provide thorough and efficient advice to our business, government, not-for-profit and private clients.</p>

    <p>Our success in advising clients on complex property matters derives from our in-depth knowledge of property laws, contracts, capital raising, construction, environment and planning, funds management, stamp duty and tax issues. We pride ourselves on providing personalised service, whether our advice is for a residential sale or purchase, or a multifaceted, multi-million dollar development project. </p>

    <br>
    <h4>ADVICE ON ALL ASPECTS OF PROPERTY LAW</h4>
    <p>Hunt &amp; Hunt's property law experts' extensive experience spans the full breadth of property sector transactions, including: </p>
    <ul>
        <li>Acquisitions and sales, including due diligence and report preparation</li>
        <li>Agribusiness</li>
        <li>Building and construction contracts</li>
        <li>Compulsory acquisition</li>
        <li>Conveyancing</li>
        <li>Crown land leasing</li>
        <li>Leasing and licensing, including retail leasing and liquor licensing</li>
        <li>Native Title</li>
        <li>Owners corporation rules and disputes</li>
        <li>Planning and environmental law</li>
        <li>Property management and development</li>
        <li>Property trusts and joint venture structures</li>
        <li>State taxes, GST and capital gains tax advice</li>
        <li>Subdivisions, including community title</li>
        <li>Syndicated property investments</li>
        <li>Valuation disputes</li>
        <li> Water law sales and purchases</li>
    </ul>
    <br>
    <h4>OUR APPROACH TO PROPERTY MATTERS</h4>
    <p>We take a commercial and practical approach to property matters. From each project's inception through to completion and ongoing management, our contribution can add value beyond our technical work. To minimise costs, our property team – including partners, senior associates and lawyers – works together to ensure the right person with the appropriate level of experience and expertise addresses your needs.
    </p>
    <p class="literature-container"> <span class="icon"><img src="/img/pdf.png" alt="Property and Projects Capability" style="width:16px;height:16px;border:0px;"></span> <span class="name"><a href="/pdf/PropertyandProjects.pdf">Property and Projects Capability</a></span> (3040 KB) </p>

    <br>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                              	Strata Law	
                            </a>
                          </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    <p>Our national team delivers practical and strategic advice on strata and community titles laws and regulations. We also provide training to strata managers, executive committees and other strata industry stakeholders.
                    </p>
                    <p>We regularly advise on the following:</p>

                    <h4>STRATA AND OWNERS CORPORATION ADVICE</h4>
                    <ul>
                        <li>Drafting strata management statements
                        </li>
                        <li>Drafting community management statements
                        </li>
                        <li>Drafting building management statements
                        </li>
                        <li>Company title advice
                        </li>
                        <li>Drafting company constitutions
                        </li>
                        <li>Attendance and scrutineering at meetings
                        </li>
                        <li>Drafting motions
                        </li>
                        <li>Advice about conduct of committee and general meetings
                        </li>
                    </ul>
                    <h4>PROPERTY AND BUILDING ADVICE</h4>
                    <ul>
                        <li>By-law drafting and registration
                        </li>
                        <li>Leases, licences and easements
                        </li>
                        <li>Strata title advice
                        </li>
                        <li>Cooperative titles advice
                        </li>
                        <li>Changing lot entitlements and lot entitlement principles
                        </li>
                        <li>Reversion of contribution schedule lot entitlements
                        </li>
                        <li>Caretaking and letting agreements
                        </li>
                        <li>Assignment of management rights
                        </li>
                        <li>Exclusive use and occupation authorities
                        </li>
                        <li>Maintenance
                        </li>
                        <li>Insurance
                        </li>
                        <li>Construction contracts
                        </li>
                    </ul>
                    <h4>DISPUTE RESOLUTION AND LITIGATION</h4>
                    <ul>
                        <li>Contractual disputes between bodies corporate and building managers
                        </li>
                        <li>Building defect claims
                        </li>
                        <li>Enforcement of by-laws
                        </li>
                        <li>Levy collection
                        </li>
                        <li>Nuisance
                        </li>
                        <li>Defamation
                        </li>
                        <li>Applications for dispute resolution before the Office of the Commissioner for Body Corporate and Community Management (QLD)
                        </li>
                        <li>Applications for dispute resolution before the Office of Fair Trading (NSW)
                        </li>
                        <li>Applications for dispute resolution before NCAT, VCAT and QCAT
                        </li>
                        <li>Applications for dispute resolution in all Courts
                        </li>
                        <li>Mediation and other alternative dispute resolution.
                        </li>
                    </ul>

                </div>
            </div>
        </div>

    </div>
