
    <p class="bigger-red-text">Hunt &amp; Hunt has extensive experience in the health sector and provides legal services to some of Australia's most prominent healthcare organisations.</p>
    <p>Hunt &amp; Hunt's in-depth understanding of the health sector means we can provide your organisation with practical, well-researched and commercially astute advice.
    </p>
    <p>We are familiar with the legal, political, social and technological issues that influence this industry, such as continuous technological innovation, Australia's ageing population and the growing cultural emphasis on healthy living. We have developed strong relationships with peak bodies and key professionals to ensure we stay abreast of the latest issues and trends.
    </p>
    <p>Our clients include allied health service providers, area health services, industry associations, public and private hospitals, specialist health providers, and one of Australia's largest health insurers.
    </p>

    <h4>CORPORATE ADVISORY SERVICES</h4>
    <p>We provide corporate and commercial advice to healthcare organisations on matters including intellectual property, mergers and acquisitions, outsourcing and tenders, public–private partnerships, research agreements and trade practices.
    </p>

    <h4>GOVERNANCE AND REGULATORY COMPLIANCE</h4>
    <p>We assist healthcare organisations with regulatory compliance; clinical and corporate governance; clinical trials, research and ethics; processing of surgical devices; and accreditation issues.
    </p>

    <h4>OPERATIONAL ISSUES</h4>
    <p>Our lawyers provide healthcare clients with advice on operational issues including capacity and consent matters, fundraising and estate issues, health insurance, and privacy of and access to medical records.
    </p>

    <h4>EMPLOYMENT</h4>
    <p>We are experts in industrial relations and employment law, and advise clients on matters including funding, medical appointments, and occupational health and safety.
    </p>

