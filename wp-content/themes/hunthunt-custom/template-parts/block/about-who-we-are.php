<p class="bigger-red-text">Our accomplished partners and lawyers pride themselves on providing easy-to-understand legal advice in a open, friendly and straightforward manner.</p>
<p>Established in 1929, Hunt &amp; Hunt is a vibrant, mid-sized law firm that provides tailored legal advice to clients across Australia and internationally. Our broad client base includes large and small businesses, government departments, major insurance firms, not-for-profit organisations and private clients.
</p>
<p>
    Our professionalism, combined with our friendly and energetic approach, has made Hunt &amp; Hunt a trusted advisor to some of Australia's largest and most diverse organisations.
</p>
<br>
<h4>PRIORITISING YOUR SUCCESS</h4>
<p>
    When you engage Hunt &amp; Hunt, you access a vast pool of resourceful lawyers who are passionate about the law and providing high-quality advice to organisations and private clients. Our lawyers are committed, capable and most importantly, they are great communicators.
    <br>
    <a href="/team" title="Meet our team">Meet our team</a>
</p>
<br>

<h4>WE MAKE A DIFFERENCE</h4>
<p>
    We make a real contribution to our clients and to the wider community. We encourage our lawyers to undertake <a href="/corporate-social-responsibility" title="">pro bono work</a>. It's also why we <a href="/not-for-profit" title="not-for-profit">support national
                    and local not-for-profit organisations</a>, including Clean Up Australia, and the Banksia Environmental Foundation.</p>