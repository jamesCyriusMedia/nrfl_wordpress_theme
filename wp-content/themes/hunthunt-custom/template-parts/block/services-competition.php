
    <p class="bigger-red-text">
        Hunt &amp; Hunt's competition and consumer law specialists offer practical, commercially astute advice to help you do business in Australia and overseas.</p>

    <p>Our lawyers have extensive experience and a commercial perspective gained from advising clients of all sizes including private individuals, small to medium-sized companies and large multinationals. </p>

    <p>Hunt &amp; Hunt's Shanghai office and our exclusive membership of Interlaw, an association of independent commercial law firms across 129 cities worldwide, give us access to additional knowledge and experience on cross-border and multi-jurisdictional transactions.</p>

    <p>Our lawyers work proactively to ensure clients understand their legal and compliance obligations before issues arise. In the event of an Australian Competition and Consumer Commission (ACCC) investigation, commercial dispute or regulatory issue, the team draws on its collective skills and experience to achieve favourable outcomes for clients.</p>

    <br>

    <h4>COMPETITION AND REGULATION</h4>
    <p>The complex challenges presented by competition (antitrust) law and its enforcement by the ACCC require proactive and highly experienced lawyers.
    </p>
    <p>Hunt &amp; Hunt's competition specialists understand the changing nature of Australia's regulatory environment. Our experience helps us anticipate clients' needs and respond with practical, workable advice that ensures compliance, and which balances a client's business situation and objectives against relevant laws and ACCC requirements.
    </p>
    <p>Our competition law experience covers:</p>
    <ul>
        <li>ACCC investigations</li>
        <li>Cartel behaviour and anti-competitive conduct such as price fixing</li>
        <li>Competition law compliance programs, reviews and training</li>
        <li>Fair trading</li>
        <li>Infrastructure access and regulation</li>
        <li>Strategic advice and planning.</li>
    </ul>
    <br>
    <h4>CONSUMER PROTECTION AND THE AUSTRALIAN CONSUMER LAW</h4>
    <p>Our national practice advises clients such as advertising and marketing agencies, manufacturers, retailers and suppliers on the competition and consumer requirements of advertising and infomercial campaigns.
    </p>
    <p>Our expertise covers:</p>
    <ul>
        <li>Advertising and promotional reviews</li>
        <li>Consumer warranties</li>
        <li>Exclusive dealing</li>
        <li>Product liability and recalls.</li>
    </ul>
    <br>
    <p class="literature-container"> <span class="icon"><img src="/img/pdf.png" alt="Commercial Advisory" style="width:16px;height:16px;border:0px;"></span> <span class="name"><a href="/pdf/CommercialAdvisory.pdf">Commercial Advisory</a></span> (2003 KB) </p>

