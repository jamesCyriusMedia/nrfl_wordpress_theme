<aside>
    <div>
        <?php $parenttitle = get_the_title( get_option('page_for_posts', true) ); ?>
        <h4 class="black-title"><?php echo $parenttitle ?></h4>
        <h4>
    <ul>
        <?php $page_url = wp_make_link_relative(get_permalink()) ?>
		<!-- <?php echo $page_url ?> -->
        <li class="<?php echo $page_url == '/resources/'? 'submenu-selected' : ''?>"><a href="/resources" title="News & Articles">News & Articles</a></li>
        <li class="<?php echo $page_url == '/resources/faqs/'? 'submenu-selected' : ''?>"><a href="/resources/faqs" title="FAQs">FAQs</a></li>
        <li class="<?php echo $page_url == '/resources/links/'? 'submenu-selected' : ''?>"><a href="/resources/links" title="Links">Links</a></li>
    </ul>
</h4></div>
</aside>