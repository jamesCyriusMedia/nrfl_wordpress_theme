
    <p class="bigger-red-text">Intellectual property (IP) law is complex and constantly evolving. Clients regularly call on our IP advisory specialists to protect their interests in issues involving confidential information, copyright, designs, patents and trade marks.</p>
    <p>Hunt &amp; Hunt's intellectual property lawyers offer fully integrated IP services and advice. Our services encompass the protection, management and enforcement of IP, including negotiating licences and other commercial agreements, dispute resolution and litigation.
    </p>
    <p>We take the time to ask practical questions to ensure our expert legal advice works hand-in-hand with the real-world commercial context of your business.</p>

    <p>Our clients come from a wide range of industry sectors including government, information technology, manufacturing and telecommunications.</p>
    <br>
    <h4>INTELLECTUAL PROPERTY IN THE DIGITAL WORLD</h4>
    <p>We develop legal strategies to maximise and protect the commercial potential of technological advances in the digital sphere. Our IP team regularly provides legal advice on issues surrounding telecommunications and broadband technologies, database protection, software licensing, e-commerce, regulatory convergence in communications and broadcasting, internet law and domain name disputes.
    </p>

    <p class="literature-container"> <span class="icon"><img src="/img/pdf.png" alt="Commercial Advisory" style="width:16px;height:16px;border:0px;"></span> <span class="name"><a href="/pdf/CommercialAdvisory.pdf">Commercial Advisory</a></span> (2003 KB) </p>

