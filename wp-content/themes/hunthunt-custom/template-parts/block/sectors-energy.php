

    <p class="bigger-red-text">Hunt &amp; Hunt has in-depth experience in energy and resources, and regularly advises Australian and international clients on the legal and commercial issues related to this booming sector.</p>
    <p>Our energy and resources team advises clients on major projects in industries such as climate change, contamination, gas, metals, mining, oil and water. We also deal with land rights issues and native title.
    </p>
    <p>To help ensure your project runs smoothly, we take time to understand your organisation's needs. Our lawyers are proactive, flexible and highly knowledgeable. With offices throughout Australia and in Shanghai, China, we provide clients with seamless legal services across different jurisdictions. Our lawyers' understanding of local cultures and business practices means you'll receive relevant and accurate advice.
    </p>

    <h4>ENSURING THE SUCCESS OF YOUR PROJECT</h4>
    <p>We work with clients at each stage of the project – from inception and development through to completion. Our lawyers provide expert advice on commercial contracting; environmental issues such as carbon reporting and climate change; exploration, production, processing, distribution and transmission agreements; and Native Title and Indigenous heritage rights.
    </p>
    <p>We can also assist with:</p>

    <ul>
        <li>commercial litigation</li>
        <li>contamination</li>
        <li>due diligence, including tenement search and reports, and advice on the Corporations Act 2001 (Cth)</li>
        <li>initial public offerings and Australian Securities Exchange (ASX) listings</li>
        <li>joint ventures</li>
        <li>mergers and acquisitions</li>
        <li>occupational health and safety</li>
        <li>project development (including planning) and financing.</li>
    </ul>

