
    <p class="bigger-red-text">The legal complexities of the environment and planning regime can be challenging.</p>
    <p>Hunt &amp; Hunt's environment and planning lawyers provide practical and strategic advice on development, compliance, planning appeals, contamination and compulsory acquisitions to a diverse group of public and private sector clients. We work with our clients to develop strategies that minimise the risks associated with litigation, investigations and prosecutions.</p>

    <p> Our environment and planning team benefits from the wider firm's extensive experience in complementary areas such as property, finance, infrastructure, major projects and commercial law.</p>

    <br>
    <h4>PLANNING</h4>
    <p>Compliance is an important part of our work. We have considerable national expertise in applying and complying with state, territory and Commonwealth regulatory frameworks relating to environment and planning matters.</p>

    <p>We act in environment audits and impact assessments, and manage environment and planning litigation and approvals. Our lawyers understand liquor licensing, land use regulations, and heritage and Native Title law, and regularly complete due diligence on potential development sites. We are also familiar with carbon trading and climate change, vegetation, biodiversity and waste management.</p>

    <p>Our lawyers regularly manage proceedings in the State Supreme Courts and Courts of Appeal, the Land and Environmen&shy;t Court of New South Wales, tribunals and The High Court of Australia.</p>

    <br>
    <h4>CONTAMINATION</h4>
    <p> We offer our clients a comprehensive understanding of the complex regulations and government policies&shy; relating to contaminated sites, including soil, water and air. Our experience in advising on contamination issues includes the statutory audit process, responsibility allocation, dealing with the relevant state or territory Environment Protection Authority, collaborating with site contamination and/or remediation consultants, advising on public consultation programs and liability advice and representation.</p>

    <p> We act in contamination litigation in the Supreme Courts of each State, the Federal Court of Australia and the Land and Environment Court of New South Wales.</p>

    <br>
    <h4>COMPULSORY ACQUISITIONS</h4>
    <p>The compulsory acquisition and planning overlay processes are covered by unique legislative regimes which differ from standard property and conveyancing law practices.</p>

    <p>Our team is well placed to advise on the application of the Land Acquisition (Just Terms Compensation) Act 1991 (NSW) and the Land Acquisition and Compensation Act 1986 (Vic), including advising on property possession, access, powers of entry, occupation, heads of compensation, engaging independent property valuers, and providing representation in valuers conferences/negotiations as well as representation in courts and tribunals.</p>

    <br>
    <h4>NUCLEAR ENERGY AND WASTE</h4>
    <p>Hunt &amp; Hunt Environment and Planning Lawyers, Maureen Peatman, Arie van der Ley and Jessica Baldwin, have contributed the Australian chapter for the Interlaw publication considering the issue of Nuclear Energy and Waste. </p>

    <p class="literature-container"> <span class="icon"><img src="/img/pdf.png" alt="Nuclear Energy and waste in Australia" style="width:16px;height:16px;border:0px;"></span> <span class="name"><a href="/pdf/Nuclear_Energy_and_waste_in_Australia.pdf">Nuclear Energy and waste in Australia</a></span> (369 KB) </p>

