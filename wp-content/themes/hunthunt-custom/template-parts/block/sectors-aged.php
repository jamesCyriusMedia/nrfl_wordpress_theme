<p class="bigger-red-text">The Australian Bureau of Statistics predicts that by 2030, around one-quarter of Australia's population will be 65 or older. To meet the growing demand for legal services in the aged care sector, Hunt &amp; Hunt has a dedicated group of lawyers specialising in this industry.</p>
    <p>Hunt &amp; Hunt has in-depth knowledge of the legislation and government policies that regulate the aged care sector. We provide strategic, tailored advice to a variety of organisations, including aged care facilities and retirement villages.</p>

    <p>Hunt &amp; Hunt understands the issues surrounding aged care industry regulations, and we regularly participate in related discussions and forums. We stay up to date with industry trends and the issues faced by organisations in this sector, ensuring we provide high-quality and commercially relevant advice.</p>
    <br>
    <h4>CORPORATE AND COMMERCIAL</h4>
    <p>We provide corporate and commercial advice on matters including mergers and acquisitions, joint ventures and dispute resolution.</p>
    <br>
    <h4>GOVERNANCE</h4>
    <p>In recent years, many established owners of retirement villages, aged care facilities and not-for-profit organisations have been acquired by new entrants, or have been forced to restructure or consolidate to remain competitive. </p>

    <p>We can advise you on regulatory compliance and corporate governance. Our lawyers can help your organisation develop clear and straightforward documentation that complies with the Aged Care Act 1997 and other regulatory requirements.</p>
    <br>
    <h4>PROPERTY</h4>
    <p>Our lawyers can advise on a range of property issues including construction, conveyance and leasing of units to residents, planning and development, and the sale and purchase of sites and villages.</p>
    <br>
    <h4>EMPLOYMENT</h4>
    <p>We are experts in industrial relations and employment law, and can advise on matters including change management, industrial relations strategy and litigation, occupational health and safety, and workplace agreements.</p>
