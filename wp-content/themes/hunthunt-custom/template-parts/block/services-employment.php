

    <p class="bigger-red-text">Hunt &amp; Hunt's employment law experts provide a truly integrated service covering the full range of employment and industrial relations obligations you may face. </p>
    <p>Our aim is to provide practical and effective solutions to help you manage your employees and workplace, and minimise your risk exposure. Our team advises on compliance with all workplace and industrial relations issues, modern awards, and workplace legislation including the Fair Work Act 2009; equal opportunity laws and discrimination; and workplace/occupational health and safety laws including critical incident management advice, policy implementation and defending prosecutions. </p>

    <p>Our team represents national employers and employer associations in various industries, across the following services:</p>

    <ul>
        <li>Adverse action/general protections claims under the Fair Work Act 2009</li>
        <li>Change management</li>
        <li>Confidentiality and intellectual property</li>
        <li>Contract carrier issues</li>
        <li>Contractor arrangements and management</li>
        <li>Disciplinary action and dismissals</li>
        <li>Discrimination and sexual harassment</li>
        <li>Downsizing and and re-sizing workforces</li>
        <li>Employee remuneration and benefits</li>
        <li>Employment and workplace management training</li>
        <li>Employment contracts</li>
        <li>Enterprise agreement development</li>
        <li>Executive entry and exit strategies</li>
        <li>Industrial relations strategy and litigation</li>
        <li>Military discipline</li>
        <li>Modern awards</li>
        <li>Policy documentation drafting and review</li>
        <li>Privacy and employee computer use</li>
        <li>Restraint of trade/non-compete</li>
        <li>Tender process advice and administrative support</li>
        <li>Transfer of business obligations and outsourcing arrangements</li>
        <li>Unfair/unlawful dismissals</li>
        <li>Workers compensation</li>
        <li>Workplace health and safety laws</li>
        <li>Workplace investigations</li>
        <li>Workplace mergers and restructures</li>
    </ul>
    <p>We publish regular articles and updates that connect developments in the law with employment practices in the workplace. Clients rely on our proactive and strategic advice, our robust and practical responses to individual and collective disputes, and for our leading-edge expertise in managing litigation and advice in new and developing areas of workplace law.</p>

    <br>
    <h4>EXPERTS IN AUSTRALIAN EMPLOYMENT LAW</h4>
    <p>We understand it is critical that you work with lawyers who are not only experts at the law, but who understand your needs, your business and the industry in which you operate. Our team is renowned for its responsiveness and sound legal advice that enables commercial decision making.
    </p>
    <p>We work closely with our clients to ensure their employment and workplace relations policies and practices are legally compliant and reflect industry best practice, but are also aligned to their business objectives. Through a combination of strategic advice and in-house training, we can help you avoid expensive and time-consuming litigation by resolving potential conflicts at the earliest possible stage. We prioritise communication and conciliation to explore early and appropriate resolution, rather than confrontation.
    </p>
    <p>If litigation or the threat of industrial disputation is unavoidable, we'll act quickly to help you reach the best and most cost-effective outcome. Our lawyers have an excellent record of successful representation.
    </p>
    <br>
    <h4>EMPLOYEE REMUNERATION AND BENEFITS</h4>
    <p>As employees are the key assets of any organisation, and profits can be heavily influenced by the resources needed to reward and incentivise your workforce, it is important for organisations to get their employee incentive programs right. Our employee remuneration and benefits specialists can help you achieve this. Find out more.

    </p>
    <p class="literature-container"> <span class="icon"><img src="/img/pdf.png" alt="Employment capability" style="width:16px;height:16px;border:0px;"></span> <span class="name"><a href="/pdf/Employment.pdf">Employment capability</a></span> (998 KB) </p>

