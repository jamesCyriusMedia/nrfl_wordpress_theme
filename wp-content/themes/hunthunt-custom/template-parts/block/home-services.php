<div class="container">
    <div class="row">
        <div class="col-sm-4">
            <h4 style="font-family: 'frutiger-lightregular', sans-serif; font-weight: bold;">Local focus, national strength</h4>
            <img class="col-image" src="<?php echo get_template_directory_uri(); ?>/inc/img/sidebar-map.jpg" alt="Our Location">
            <p class="column-para">Hunt & Hunt North Ryde is part of a national group, servicing large corporations to small businesses, not for profit organisations and individuals. Our specialist family law group take care of local families, serving a wide radius of the Northen Suburbs around Macquarie Park.</p>
            <a class="findOutMore" href="/contact-us">Find Out More</a>
        </div>
        <div class="col-sm-4">
            <h4 style="font-family: 'frutiger-lightregular', sans-serif; font-weight: bold;">Meet our people</h4>
            <img class="col-image" src="<?php echo get_template_directory_uri(); ?>/inc/img/Ian-Miller-full.jpg" alt="Ian Miller Image">
            <p class="column-para">Ian Miller has 35 years’ experience and his proven skills in property, commercial and company law are invaluable to his clients. Using his intimate knowledge of the property industry, Ian creates outcome-focused results for his clients. In 2019 Ian was awarded an Order of Australia Medal (OAM) for....</p>
            <a class="findOutMore" href="/contact-us">Find Out More</a>
        </div>
        <div class="col-sm-4">
            <h4 style="font-family: 'frutiger-lightregular', sans-serif; font-weight: bold;">Latest News</h4>
            <img class="col-image" src="<?php echo get_template_directory_uri(); ?>/inc/img/newspaper-coffee.jpg">
            <?php 
            // the query
            $the_query = new WP_Query( array(
                'posts_per_page' => 6,
            )); 
            ?>

            <?php if ( $the_query->have_posts() ) : ?>
            <ul id="homepage-posts">
            <aside class="blog-post">
                <ul style="padding-left: 0px;">
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
            <?php endwhile; ?>
                </ul>
            </aside>
            <?php wp_reset_postdata(); ?>

            <?php else : ?>
            <p><?php __('No News'); ?></p>
            <?php endif; ?>
        </div>

                    <!-- <div class="col-sm-3">
                        <a href="/practice-areas/asset-protection-and-succession-planning" title="ASSET PROTECTION SUCCESSION PLANNING">
                            <img src="<?php echo get_template_directory_uri(); ?>/inc/img/shield-icon.png" alt="ASSET PROTECTION SUCCESSION PLANNING icon">
                            <h4>ASSET PROTECTION,<br>SUCCESSION PLANNING</h4>
                            <p> Services and advice to help you manage and safeguard what you've worked hard to achieve. </p>
                        </a>
                    </div>

                    <div class="col-sm-3">
                        <a href="/practice-areas/corporate-and-commercial" title="Corporate and commercial">
                            <img src="<?php echo get_template_directory_uri(); ?>/inc/img/briefcase-icon.png" alt="CORPORATE &amp; COMMERCIAL LAW icon">
                            <h4>CORPORATE &amp; <br>COMMERCIAL LAW</h4>
                            <p> Commercially astute advice to suit the business environment in which you operate. </p>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="/practice-areas/employment-and-workplace-relations" title="Employment and workplace relations">
                            <img src="<?php echo get_template_directory_uri(); ?>/inc/img/seekpeople-icon.png" alt="EMPLOYMENT LAW icon">
                            <h4>EMPLOYMENT LAW</h4>
                            <p> A truly integrated service covering the full range of employment and industrial relations obligations you may face. </p>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="/practice-areas/wills-and-estates-planning" title="Wills and estates planning">
                            <img src="<?php echo get_template_directory_uri(); ?>/inc/img/contract-icon.png" alt="WILLS &amp; ESTATESG icon">
                            <h4>WILLS &amp; ESTATES</h4>
                            <p> Ensure your estate is distributed according to your wishes. </p>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <a href="/practice-areas/litigation-and-dispute-resolution" title="LITIGATION, DISPUTE RESOLUTIONG">
                            <img src="<?php echo get_template_directory_uri(); ?>/inc/img/hammer-icon.png" alt="LITIGATION, DISPUTE RESOLUTIONG icon">
                            <h4>LITIGATION, DISPUTE<br>RESOLUTION</h4>
                            <p> Resolve commercial disputes with minimal impact on your business. </p>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="/practice-areas/mergers-and-acquisitions" title="MERGERS &amp; ACQUISITIONS">
                            <img src="<?php echo get_template_directory_uri(); ?>/inc/img/plus-icon.png" alt="MERGERS &amp; ACQUISITIONS icon">
                            <h4>MERGERS &amp; <br>ACQUISITIONS</h4>
                            <p> Depth of experience in advising clients on mergers, acquisitions, disposals and joint ventures. </p>
                         </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="/practice-areas/property-law" title="PROPERTY LAW">
                        <img src="<?php echo get_template_directory_uri(); ?>/inc/img/house-icon.png" alt="PROPERTY LAW icon">
                        <h4>PROPERTY LAW</h4>
                        <p> Thorough and efficient advice to our business, government, not-for-profit and private clients. </p>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="/practice-areas/asset-protection-and-succession-planning" title="ASSET PROTECTION SUCCESSION PLANNING">
                            <img src="<?php echo get_template_directory_uri(); ?>/inc/img/arrow-icon.png" alt="SEE ALL OF OUR SERVICES icon">
                            <h4>SEE ALL OF <br>OUR SERVICES</h4>
                        </a>
                    </div> -->
                </div>
            </div>