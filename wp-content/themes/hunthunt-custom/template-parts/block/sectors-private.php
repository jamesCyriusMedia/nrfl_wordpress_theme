
    <p class="bigger-red-text">At some point in our lives, most of us will need clear, sound advice from legal experts who prioritise our best interests.</p>

    <p>Hunt &amp; Hunt provides comprehensive and cost-effective legal services to individuals. Whether you are embarking on a new venture, resolving conflict or managing and protecting your personal wealth, we can provide sound advice and guidance.</p>

    <p>You'll benefit from our experience and our dedication to helping clients achieve the best results. We'll listen to your needs and then provide practical and straightforward recommendations – without the legal speak – to guide you through these often unfamiliar processes.</p>

    <br>
    <h4>SUPPORTING CLIENTS WITH PERSONALISED LEGAL ADVICE</h4>
    <p>Hunt &amp; Hunt provides high-quality legal advice tailored to your circumstances, so you can make informed decisions. </p>

    <p>We provide advice on all areas of family law – including separation and divorce, child custody and support, and pre-nuptial agreements – as well as commercial law, including starting a business, drafting for a franchise agreement or negotiating a contract. We represent clients in court during litigation and work quickly and support you every step of the way.</p>

    <p>We also assist with wills and estate planning. We advise clients on property-related matters including the purchase and sale of real estate and businesses, and provide financing advice for borrowers and lenders, leasing and land community title, and strata subdivision.</p>
    <p class="literature-container"> <span class="icon"><img src="/img/pdf.png" alt="Personal Services Brochure" style="width:16px;height:16px;border:0px;"></span> <span class="name"><a href="/pdf/Personal+Services+Brochure.pdf">Personal Services Brochure</a></span> (851 KB) </p>
    <p class="literature-container"> <span class="icon"><img src="/img/pdf.png" alt="Selling a Business / Business Exits" style="width:16px;height:16px;border:0px;"></span> <span class="name"><a href="/pdf/BusinessExits.pdf">Selling a Business / Business Exits</a></span> (280 KB) </p>
    <p class="literature-container"> <span class="icon"><img src="/img/pdf.png" alt="Can a family constitution help your family business thrive?" style="width:16px;height:16px;border:0px;"></span> <span class="name"><a href="/pdf/2015-02-11FamilyConstitution.pdf">Can a family constitution help your family business thrive?</a></span> (285 KB) </p>

