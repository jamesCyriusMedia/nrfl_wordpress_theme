<div class="carousel-zone container">
    <div class="home-carousel" role="toolbar">                
        <div class="item  slick-slide" data-slick-index="0" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide00" style="width: 760px; position: relative; left: 0px; top: 0px; z-index: 998; opacity: 0; transition: opacity 500ms linear 0s;">
            <a href="/services/divorce-and-separation" title="Divorce and Separation"><h1>Divorce &amp; Separation</h1></a>
            <hr>
            <p style="color: #fff;">We understand that family disputes are difficult for everyone involved, so we work quickly to support you every step of the way.</p>
        </div>
        <div class="item slick-slide" data-slick-index="1" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide01" style="width: 760px; position: relative; left: -760px; top: 0px; z-index: 998; opacity: 0; transition: opacity 500ms linear 0s;">
            <a href="/services/property-settlement" title="Property Settlement"><h1>Property Settlement</h1></a>
            <hr>
            <p style="color: #fff;">A family break-up can involve complex settlements involving property and other assets. We approach every settlement with care and fairness.</p>
        </div>
        <div class="item slick-slide" data-slick-index="2" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide02" style="width: 760px; position: relative; left: -1520px; top: 0px; z-index: 998; opacity: 0; transition: opacity 500ms linear 0s;">
            <a href="/services/spouse-maintenance-and-child-support" title="Spouse Maintenance and Child Support"><h1>Spousal Maintenance &amp; Child Support</h1></a>
            <hr>
            <p style="color: #fff;">It is critical to ensure your children are taken care of adequately and with as much stability as possible. Both parents have a vital role to play.</p>
        </div>
        <div class="item slick-slide" data-slick-index="3" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide03" style="width: 760px; position: relative; left: -2280px; top: 0px; z-index: 998; opacity: 0; transition: opacity 500ms linear 0s;">
            <a href="/services/parenting/" title="Parenting"><h1>Parenting</h1></a>
            <hr>
            <p style="color: #fff;">Every family situation is different when it comes to parenting in a separation. It is important to look after the best interests of your children.</p>
        </div>
        <div class="item slick-slide slick-current slick-active" data-slick-index="4" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide04" style="width: 760px; position: relative; left: -3040px; top: 0px; z-index: 999; opacity: 1;">
            <a href="/services/financial-agreements" title="Financial Agreements"><h1>Financial Agreements</h1></a>
            <hr>
            <p style="color: #fff;">It pays to plan ahead, to protect your family’s future. We can advise you and prepare agreements that maximise your financial security.</p>
        </div>
        <div class="item slick-slide slick-current slick-active" data-slick-index="5" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide05" style="width: 760px; position: relative; left: -3800px; top: 0px; z-index: 999; opacity: 1;">
            <a href="/services/wills-and-estate-planning" title="Wills and Estate Planning"><h1>Wills &amp; Estate Planning</h1></a>
            <hr>
            <p style="color: #fff;">With the right advice and careful planning, we can help assure that even the most complex estates can be managed appropriately for all concerned.</p>
        </div>                              
    </div>
</div>