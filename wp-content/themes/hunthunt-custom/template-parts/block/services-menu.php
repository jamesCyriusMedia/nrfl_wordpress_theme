<aside>
    <div>
		<?php $parenttitle = get_the_title($post->post_parent); ?>
        <h4 class="black-title"><?php echo $parenttitle ?></h4>
        <h4>

    <ul>
		<?php $page_url = wp_make_link_relative(get_permalink()) ?>
		<!-- <?php echo $page_url ?> -->
		<li class="<?php echo $page_url == '/services/'? 'submenu-selected' : ''?>"><a href="/services" title="Services Overview">Services Overview</a></li>
		<li class="<?php echo $page_url == '/services/divorce-and-separation/'? 'submenu-selected' : ''?>"><a href="/services/divorce-and-separation" title="Divorce & Separation">Divorce & Separation</a></li>
		<li class="<?php echo $page_url == '/services/property-settlement/'? 'submenu-selected' : ''?>"><a href="/services/property-settlement" title="Property Settlement">Property Settlement</a></li>
		<li class="<?php echo $page_url == '/services/spouse-maintenance-and-child-support/'? 'submenu-selected' : ''?>"><a href="/services/spouse-maintenance-and-child-support" title="Spouse Maintenance & Child Support">Spouse Maintenance & Child Support</a></li>
		<li class="<?php echo $page_url == '/services/parenting/'? 'submenu-selected' : ''?>"><a href="/services/parenting" title="Parenting">Parenting</a></li>
		<li class="<?php echo $page_url == '/services/financial-agreements/'? 'submenu-selected' : ''?>"><a href="/services/financial-agreements" title="Financial Agreements">Financial Agreements</a></li>
		<li class="<?php echo $page_url == '/services/wills-and-estate-planning/'? 'submenu-selected' : ''?>"><a href="/services/wills-and-estate-planning" title="Wills-and-estate-planning">Wills & Estates Planning</a></li>		
    </ul>
</h4></div>
</aside>