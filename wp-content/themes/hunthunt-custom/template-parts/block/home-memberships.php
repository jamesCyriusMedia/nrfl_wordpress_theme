<div class="container">
    <a href="/about-us/membership-association/">
        <div>
            <h3>HUNT&amp;HUNT MEMBERSHIPS &amp; ASSOCIATIONS</h3>
            <div>
                <img src="<?php echo get_template_directory_uri(); ?>/inc/img/riverside-chamber.jpg" alt="Ryde Macquarie Park logo">
                <img src="<?php echo get_template_directory_uri(); ?>/inc/img/interlaw-logo.jpg" alt="Interlaw logo">              
                <img src="<?php echo get_template_directory_uri(); ?>/inc/img/law-society-logo.jpg" alt="The Law Society logo">
                <img src="<?php echo get_template_directory_uri(); ?>/inc/img/australian-legal-logo.jpg" alt="Australian Legal Sector Alliance logo">
                <img src="<?php echo get_template_directory_uri(); ?>/inc/img/family-law-logo.jpg" alt="Family Law Section logo">
            </div>
        </div>
    </a>
</div>