

    <h4>A PROUD HERITAGE; A BRIGHT FUTURE</h4>
    <p>
        Hunt &amp; Hunt was established in 1929 by brothers Hector (Bob) and Edward (Ted) Hunt. The firm started out as an insurance and conveyancing law firm, just as Australia was entering the Great Depression.
    </p>
    <p>
        In the years leading up to World War II, Hunt &amp; Hunt expanded the team at its Sydney office. During the war, the firm's insurance and workers compensation practices boomed, largely due to the firm's association with the National Employers Mutual General Insurance Company of London. After the war, migrants arrived and settled in Sydney, starting a period of post-war prosperity that saw the firm's conveyancing practice grow rapidly.
    </p>
    <p>
        Hunt &amp; Hunt remained a family-run firm throughout this period, and in the early 1950s Ted's sons Ian and David joined the firm. In 1956, Ted was elected mayor of Parramatta Council in Sydney's western suburbs.
    </p>
    <p>
        Over the following decades, Hunt &amp; Hunt expanded interstate and overseas by acquiring smaller practices and entering into strategic alliances.
    </p>
    <p>
        Today, our firm has a strong national footprint with approximately 400 staff, including 59 partners. <a href="/team" title="Meet our team">Meet our team</a>.
    </p>
    <br>
    <h4>INTERLAW</h4>
    <p>
        Hunt &amp; Hunt is the exclusive Interlaw member firm of Australia. Interlaw is the preferred law firm network for companies doing business internationally.
    </p>
    <p>
        Interlaw has earned the highest professional rankings as an “Elite global law firm network”* and counts as its members top tier independent law firms in 130 cities worldwide. Interlawyers subscribe to the core values of impeccable ethics, international standards of transparency, and devotion to client service. They are fluent in both their own native language(s) as well as business English.
    </p>
    <p>
        Rely on Interlawyers to responsibly, effectively and professionally navigate the rules, regulations and procedures for transacting matters locally and abroad. Interlaw is highly selective and member firms must demonstrate leadership within their jurisdiction and a proven track record of success with domestic and international legal matters before being invited to join.
    </p>
    <br> * As ranked by Chambers &amp; Partners
