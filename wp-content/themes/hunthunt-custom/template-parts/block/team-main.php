<div class="row">
	<div class="col-lg-2 col-sm-3 ">
		<img class="team-thumbnail" src="/img/profiles/Joe-Dominello-thumb.jpg" alt="Joe Dominello picture">
	</div>
	<div class="col-lg-10 col-sm-9 " style="margin-bottom: 20px;">
		<h4 style="text-transform:uppercase">Joe Dominello</h4>
        <h3>Partner/Principal</h3>
		<p class="gr-txt">Joe has an extensive career as a solicitor spanning more than 40 years and practicing in the area of property law, wills and will disputes.<br>
			Joe joined Hunt & Hunt in 2020, when Bond & Bond integrated with the firm.<br>
			Throughout his career, Joe has served time on the Law Society Elder Law and Property Law Committees.<br>
			View more details on our parent website <a href="https://www.hunthunt.com.au/people/joe-dominello/" target="_blank" class="lnk lnk2"> Here &gt; </a>
		</p>
    </div>
</div>
<div class="row">
	<div class="col-sm-12">
		<hr>
	</div>
</div>
<div class="row">
	<div class="col-lg-2 col-sm-3 ">
		<img class="team-thumbnail" src="/img/profiles/Ian-Miller-thumb.jpg" alt="Ian Miller picture">
	</div>
	<div class="col-lg-10 col-sm-9 " style="margin-bottom: 20px;">
		<h4 style="text-transform:uppercase">Ian Miller OAM</h4>
        <h3>Consultant</h3>
		<p class="gr-txt">With over 35 years’ experience, Ian’s proven skills in property, commercial and company law are invaluable to his clients. Ian’s experience also covers trade practices, employment law and laws relevant to charitable institutions.<br>
			Using his intimate knowledge of the property industry, Ian creates outcome-focused results for his clients. His competencies range from advising on mortgage documents and commercial leasing to providing commercial and legal advice to small – medium sized businesses.<br>
			View more details on our parent website <a href="https://www.hunthunt.com.au/people/ian-miller/" target="_blank" class="lnk lnk2"> Here &gt; </a>
		</p>
    </div>
</div>
<div class="row">
	<div class="col-sm-12">
		<hr>
	</div>
</div>
<div class="row">
	<div class="col-lg-2 col-sm-3 ">
		<img class="team-thumbnail" src="/img/profiles/Craig-Gallagher-thumb.jpg" alt="Craig Gallagher picture">
	</div>
	<div class="col-lg-10 col-sm-9 " style="margin-bottom: 20px;">
		<h4 style="text-transform:uppercase">Craig Gallagher</h4>
        <h3>Consultant</h3>
		<p class="gr-txt">An accessible and responsive approach, combined with pragmatic advice that considers the broader commercial, technical and regulatory context puts Craig at the forefront of his profession.<br>
			View more details on our parent website <a href="https://www.hunthunt.com.au/people/craig-gallagher/" target="_blank" class="lnk lnk2"> Here &gt; </a>
		</p>
    </div>
</div>
<div class="row">
	<div class="col-sm-12">
		<hr>
	</div>
</div>
<div class="row">
	<div class="col-lg-2 col-sm-3 ">
		<img class="team-thumbnail" src="/img/profiles/Alex-Xenos-thumb.jpg" alt="Alex Xenos picture">
	</div>
	<div class="col-lg-10 col-sm-9 " style="margin-bottom: 20px;">
		<h4 style="text-transform:uppercase">Alex Xenos</h4>
        <h3>Senior Associate</h3>
		<p class="gr-txt">Alex has experience in assisting with building disputes, strata plan disputes, business litigation and commercial disputes for medium sized businesses, debt recovery matters, banking matters settlement of commercial disputes through mediations and settlement conferences.<br>
			View more details on our parent website <a href="https://www.hunthunt.com.au/people/alex-xenos/" target="_blank" class="lnk lnk2"> Here &gt; </a>
		</p>
    </div>
</div>
<div class="row">
	<div class="col-sm-12">
		<hr>
	</div>
</div>
<div class="row">
	<div class="col-lg-2 col-sm-3 ">
		<img class="team-thumbnail" src="/img/profiles/Anthea-Tronson-thumb.jpg" alt="Anthea Tronson picture">
	</div>
	<div class="col-lg-10 col-sm-9 " style="margin-bottom: 20px;">
		<h4 style="text-transform:uppercase">Anthea Tronson</h4>
        <h3>Special Counsel</h3>
		<p class="gr-txt">Starting her career as a Graduate with Hunt & Hunt in 2002, Anthea now practices in general and commercial litigation, with a focus on estate litigation.<br>
			View more details on our parent website <a href="https://www.hunthunt.com.au/people/anthea-tronson/" target="_blank" class="lnk lnk2"> Here &gt; </a>
		</p>
    </div>
</div>
<div class="row">
	<div class="col-sm-12">
		<hr>
	</div>
</div>
<div class="row">
	<div class="col-lg-2 col-sm-3 ">
		<img class="team-thumbnail" src="/img/profiles/Tim-Story-thumb.jpg" alt="Tim Story picture">
	</div>
	<div class="col-lg-10 col-sm-9 " style="margin-bottom: 20px;">
		<h4 style="text-transform:uppercase">Tim Story</h4>
        <h3>Special Counsel</h3>
		<p class="gr-txt">Tim is recognised for his expertise in the fields of property law and estate planning.<br>
Tim joined Hunt & Hunt as a law clerk in 2007, graduated in 2014, was promoted to Senior Associate in 2018 and Special Counsel in 2020.<br>
			View more details on our parent website <a href="https://www.hunthunt.com.au/people/tim-story/" target="_blank" class="lnk lnk2"> Here &gt; </a>
		</p>
    </div>
</div>
<div class="row">
	<div class="col-sm-12">
		<hr>
	</div>
</div>