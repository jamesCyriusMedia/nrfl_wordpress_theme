
    <br>
    <p> As the global agribusiness sector evolves to meet the demands of a growing population, Hunt &amp; Hunt has been successfully working alongside companies to create awareness and understanding of the legal impacts.</p>
    <p>Our experience in this sector began 80 years ago. Our knowledge and experience in this sector mean we are well-positioned to protect and advance the interest of investors, operators and processors in Australia.</p>
    <p>The services we provide include advising on the legal aspects of agribusiness operations, acquiring rural land, foreign investment regulation, export and import controls and regulatory issues.</p>
    <p>As a major exporter for food and textiles worldwide and due to its proximity to markets particularly in China, Australia has witnessed an increase in demand for Australian food exported. As a result, we have significant experience advising clients who trade and invest in China, and Chinese companies investing and trading in Australia, dating back to the 1980s.</p>
    <br>
    <h4>COMPREHENSIVE AGRIBUSINESS EXPERIENCE</h4>
    <p>No longer characterised just by family farms, the Australian agribusiness market spans businesses ranging from the production and trading of commodities such as meat, fish and grain processors; textile and food manufacturers; and agricultural machinery sellers, through to all different types of farming and specialist services such as management consulting and technical services.</p>
    <p>Our service offering reflects this advancement in sophistication and covers all areas of agricultural operations. The types of businesses we provide legal advice to includes:</p>
    <ul>
        <li>Wool</li>
        <li>Dairy </li>
        <li>Beef and Sheep livestock operations </li>
        <li>Blood stock industry/ thoroughbred </li>
        <li>Fishing </li>
        <li>Forestry</li>
        <li>Wine </li>
        <li>Cotton </li>
        <li>Rural Finance </li>
        <li>Rice, and </li>
        <li>All other livestock, crop growing and processing operations</li>
    </ul>
    <p>Across these areas of the agribusiness sector, our legal team advises clients on a range of services, including:</p>
    <ul>
        <li>Acquisition sales, joint ventures and management </li>
        <li>Trading agreements both national and internationally</li>
        <li>Corporate governance </li>
        <li>Research and development</li>
        <li>Intellectual property</li>
        <li>Disputes</li>
        <li>Employment and migration</li>
        <li>Rural conveyancing, and</li>
        <li>Succession planning for family owned farms and vineyards</li>
    </ul>

    <br>
    <h4>CONNECTION TO THE LAND</h4>
    <p>Important to our clients is our practitioners deep connections to the land. These connections run well beyond their practicing of the law.</p>
    <p>The team’s deep understanding of the realities, challenges and opportunities in the agribusiness sector, their longevity in the market and ability to deliver cost-effective practical solutions for clients is second to none. </p>
    <br>
    <p class="literature-container"> <span class="icon"><img src="/img/pdf.png" alt="Agribusiness capability" style="width:16px;height:16px;border:0px;"></span> <span class="name"><a href="/pdf/AgribusinessChinese.pdf">Agribusiness capability</a></span> (5486 KB) </p>
    <p class="literature-container"> <span class="icon"><img src="/img/pdf.png" alt="China Australia FTA" style="width:16px;height:16px;border:0px;"></span> <span class="name"><a href="/pdf/FTAandAgribusiness.pdf">China Australia FTA</a></span> (179 KB) </p>

    <br>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">                               China-Australia agribusiness                              </a> </h4> </div>
            <div id="collapseOne" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    <h4>LETS GO AGRIBUSINESS INVESTMENT SEMINARS IN TIANJIN AND BEIJING</h4>
                    <p>In November 2015 Hunt &amp; Hunt joined CBRE, Elders International, Austrade and the Victorian and Queensland State Governments in presenting Australian agribusiness investment seminars in Tianjin and Beijing. The seminars attracted more than 100 Chinese investors from sectors including beef, dairy, cropping, animal genetics, distribution logistics and investment banking. </p>
                    <p>The seminars were hosted by Lets Go (an agribusiness marketing and resources company) and formed part of a six day networking and educational tour in North East China where Australian agents and exporters are introduced to qualified Chinese agribusiness investors and importers. </p>
                    <br><img src="/img/LetsGoConference.jpg" class="/img-fluid" alt="conference">
                    <br>
                    <br>
                    <p>Mathew Alderson, Special Counsel at Hunt &amp; Hunt based in Beijing, gave a presentation and placed emphasis on the need for Chinese investors to take legal advice in advance and to appreciate the role of FIRB and the zoning and other restrictions imposed by local governments. His presentation was well received. </p>
                    <h4>CHINA-AUSTRALIA AGRIBUSINESS INVESTORS HUB</h4>
                    <p>The seminars also saw the launch of Lets Go's marketing portal www.e-LetsGo.com – it's an online hub in both Chinese and English that introduces buyers with sellers. Lets Go hosted the seminars and is a Chinese agribusiness marketing and resources company located in Tianjin, 130 km south east of Beijing. </p>
                    <img src="/img/LetsGo.jpg" class="/img-fluid" alt="seminars">
                </div>
            </div>
        </div>
    </div>
