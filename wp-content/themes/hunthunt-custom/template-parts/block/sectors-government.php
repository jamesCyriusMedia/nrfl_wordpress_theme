
    <p class="bigger-red-text">Hunt &amp; Hunt's long-term relationships with many federal, state and local government clients help us understand the unique challenges faced by clients in this sector.</p>
    <p>We understand government clients are subject to intense scrutiny, and must always be seen to act fairly, reasonably and in the best interests of the public. </p>
    <p>Hunt &amp; Hunt's private sector lawyers have extensive experience working with government clients. Our team delivers transparent, accountable and timely legal services to numerous government organisations. </p>
    <p>We appreciate that value for money is paramount for our public sector clients, which is why we carefully manage our overheads, keep you informed of legal spending and seek approval before carrying out work. All matters are managed to ensure cost-efficiency and we always ensure the most appropriate person handles your requirements. </p>
    <br>
    <h4>QUALITY LEGAL SUPPORT FOR ALL LEVELS OF GOVERNMENT</h4>
    <p>Hunt &amp; Hunt provides valuable and practical legal advice at all levels of government in a wide variety of areas, including administrative law; commercial and contract law; commercial projects and tenders; statutory interpretation of acts, regulations and local laws; and workplace law. </p>
    <p>Find out more about our work for: </p>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                              Commonwealth and state
                            </a>
                          </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    <p>Hunt &amp; Hunt’s long-term experience working with many Commonwealth and State government clients means we truly understand the business of government. In fact, our government clients rely on our ability to blend commercial goals with public sector accountability, transparency and probity.</p>
                    <p>We understand the commercial and political pressures faced by the government. Our focus is to deliver timely, cost-effective and politically sensitive legal advice. And, by having a strong national team, we provide the intimate knowledge required of Commonwealth, State and Territory laws.
                    </p>
                    <p>We represent, or have provided services to, government departments and agencies, public authorities and governmental bodies from a broad section of portfolios. The benefit to our clients is that we can easily draw upon a wealth of knowledge to ensure you receive practical and cost-effective advice.
                    </p>
                    <p>Our experience with Commonwealth and State agencies includes:</p>
                    <ul>
                        <li>Government tendering</li>
                        <li>Public administration and governance</li>
                        <li>Criminal and regulatory prosecution</li>
                        <li>Litigation</li>
                        <li>Property, including acquisitions, disposals, leases, licences and Crown land issues</li>
                        <li>Employment</li>
                        <li>Insurance</li>
                        <li>Statutory interpretation</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                              Local governments
                            </a>
                          </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                    <p>Local government law is a focal point at Hunt &amp; Hunt. To ensure our local government clients continue to operate transparently and with the utmost integrity, we work closely with them to solve legal issues and enhance governance processes.</p>
                    <p>Hunt &amp; Hunt's reputation for outstanding legal work is demonstrated by our longstanding relationships with local government clients and our many successful panel appointments.
                    </p>
                    <p>We currently act for three capital city councils and more than 50 metropolitan and regional councils across Australia. This experience means there are few municipal legal issues we haven't encountered and successfully resolved.
                    </p>
                    <p>Our practice isn't limited to acting for local governments. We also advise private sector organisations on local governmental and regulatory matters.

                    </p>
                    <br>
                    <h4>Property and planning advice</h4>
                    <p>We regularly provide advice on commercial projects, commercial agreements, planning appeals and processes, and tenders. We also work closely with local governments on property matters including development agreements, contracts, leases, valuation appeals, use and development of Crown land, and native title.
                    </p>

                    <br>
                    <h4>Compliance, litigation and strategic consultation</h4>
                    <p>We assess and manage our clients' legal risks and offer strategic advice on:
                    </p>
                    <ul>
                        <li>commercial litigation</li>
                        <li>prosecutions</li>
                        <li>public and professional liability</li>
                        <li>regulatory compliance and enforcement</li>
                        <li>statutory interpretation of acts, regulations and local laws</li>
                        <li>workplace law, including employment, industrial relations, occupational health and safety, and equal opportunity.</li>
                    </ul>
                    <p>We undertake prosecutions for local government clients and defend private clients who face enforcement action.</p>

                    <br>
                    <h4>Cost-conscious legal services and advice for local governments</h4>
                    <p>We carefully manage our overheads, keep you abreast of legal spend and seek approval before beginning work. By understanding your needs and the issues involved, we deliver relevant, practical advice for a fair legal fee.
                    </p>

                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                              Administrative reviews and disciplinary action	
                            </a>
                          </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">
                    <p>We regularly advise and represent our corporate and government clients in administrative proceedings before the Supreme Court, local courts, the Administrative Appeals Tribunal and the Administrative Decisions Tribunal of New South Wales. By forging close and long-standing relationships with clients, Hunt &amp; Hunt has established a formidable track record in administrative proceedings.
                    </p>
                    <p> For more than 20 years we have advised the NSW Roads and Maritime Services (previously NSW Roads and Traffic Authority) on its internal administrative decision-making processes. During this time, Hunt &amp; Hunt has represented the NSW Roads and Maritime Services in several thousand administrative reviews.
                    </p>
                    <p> Members of our legal team are appointed to the NSW Legal Representation Office, which was established by the NSW Government to provide independent legal assistance to people in their dealings with the NSW Police Integrity Commission (PIC) and the NSW Independent Commission Against Corruption (ICAC). Our lawyers frequently represent witnesses giving evidence before the PIC and ICAC.
                    </p>
                    <p> We are experienced in the powers and processes applied by inquiries to order the production of documents and witness evidence, take live evidence, restrict public access, enforce powers and publish and issue reports.
                    </p>
                    <p>The expertise of our lawyers also spans judicial reviews and merit reviews of administrative decisions, probity services, Freedom of Information, inquests and privacy.
                    </p>
                </div>
            </div>
        </div>
    </div>
