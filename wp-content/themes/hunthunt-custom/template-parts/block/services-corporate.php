<p class="bigger-red-text">
    To make informed and accurate business decisions, you need well-researched and tailored legal advice. Hunt &amp; Hunt's commercial lawyers work closely with you to deliver commercially astute advice to suit the business environment in which you operate.</p>

<p>Our experienced corporate and commercial lawyers provide essential day-to-day legal support and strategic advice so you can quickly manage any opportunities or issues that arise in your business.</p>

<p>We work with some of Australia's largest, most successful and fastest growing companies, as well as small and medium-sized family-owned and private businesses, and not-for-profit organisations. </p>
<p>
    Our team members are knowledgeable, responsive and passionate about what they do. They deliver practical advice in a clear, straightforward way to support your business.</p>
<br>
<h4>THE EXPERTS IN COMMERCIAL AND CORPORATIONS LAW</h4>
<p>We work with organisations across Australia and the Asia-Pacific region, providing services to assist businesses throughout their lifecycle – including incorporation and acquisition, general trading, sale and succession planning. </p>

<p>Our corporate lawyers advise on company and business acquisition and sales, including due diligence, mergers and acquisitions, company structuring and restructuring. As your business evolves, we can help you implement a business succession plan and advise on tax-effective business structures, generally including partnerships and joint ventures. </p>

<p>We also offer expertise in:</p>
<ul>
    <li>Asset security</li>
    <li>Corporate governance</li>
    <li>Customs law and practice</li>
    <li>E-commerce</li>
    <li>Franchising</li>
    <li>Initial public offerings and capital raisings</li>
    <li>Information and communications technology</li>
    <li>Intellectual property</li>
    <li>International trade and investment</li>
    <li>Mortgage broking</li>
    <li>Privacy</li>
    <li>Stamp duties and tax</li>
    <li>Supply, manufacturing, agency, licensing and distribution contracts</li>
    <li>Trade practices, fair trading and consumer law obligations</li>
    <li> Trusts</li>
</ul>
<p class="literature-container"> <span class="icon"><img src="/img/pdf.png" alt="Commercial Advisory" style="width:16px;height:16px;border:0px;"></span> <span class="name"><a href="/pdf/CommercialAdvisory.pdf">Commercial Advisory</a></span> (2003 KB) </p>
