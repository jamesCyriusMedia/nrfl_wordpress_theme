
    <p class="bigger-red-text">There's more to estate planning than simply having an up-to-date Will. Our team can help ensure your estate is distributed according to your wishes.</p>
    <p>Hunt &amp; Hunt can help protect your assets during your lifetime and the lifetime of your beneficiaries. We'll develop a tailored legal solution that complements your personal and financial goals.</p>

    <br>
    <h4>WILLS</h4>
    <p>Preparing a Will and knowing it is properly constructed will give you peace of mind and ensure your assets are managed exactly as you intend. </p>

    <p>We'll construct your Will with regard to the type and nature of your assets, and consider other factors such as whether a testamentary trust is more appropriate for your circumstances.
    </p>
    <br>
    <h4>ESTATE PLANNING</h4>
    <p>Life can be unpredictable and it's important to prepare for the unexpected. Our team can prepare a comprehensive estate plan that ensures you retain control of your assets, and decide who'll make decisions on your behalf in the event of physical or mental disability, illness, injury or death.
    </p>
    <p>We'll help you make informed decisions about estate planning issues, such as whether you should implement an enduring power of attorney or guardianship. We'll then draft the necessary documentation to put your plan into effect. Our lawyers can also provide expert advice and assistance on all aspects of deceased estates.</p>

