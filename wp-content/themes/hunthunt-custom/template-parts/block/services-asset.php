
    <p class="bigger-red-text">In business and in your personal life, you want peace of mind that your assets are protected. Hunt &amp; Hunt's lawyers provide services and advice to help you manage and safeguard what you've worked hard to achieve.
    </p>
    <p>Hunt &amp; Hunt works with private and business clients to help protect assets, advise on wills and estate planning, and develop sound business succession plans. </p>
    <p>Our experienced lawyers will always go the extra step to ensure you have maximum control of your assets. We'll also provide you with tools to help you achieve your wealth creation goals.</p>
    <br>
    <h4>TRUSTS AND SUPERANNUATION FUNDS</h4>
    <p>Owning assets through a trust provides a high level of protection from risk. Our expert lawyers can develop the legal framework and documents necessary to establish discretionary trusts, unit trusts and self managed superannuation funds. We also advise on family and charitable trusts and foundations. </p>
    <p>We can help you manage superannuation issues including compliance and applications to the Superannuation Complaints Tribunal. To secure the best results, Hunt &amp; Hunt will work closely with your accountant and financial planner. </p>
    <br>
    <h4>WILLS AND ESTATE PLANNING</h4>
    <p>Our lawyers prepare wills, codicils, powers of attorney, enduring powers of attorney and advanced health directives for clients. We also offer estate planning services including: </p>
    <ul>
        <li>advising on structuring asset holdings for individuals, families and businesses</li>
        <li>preparing family trusts</li>
        <li>preparing grants of probate</li>
        <li>managing the administration of deceased estates</li>
        <li>advising on superannuation</li>
    </ul>
    <p>Our alternative dispute resolution specialists often help clients through difficult circumstances, such as conflicts in estate matters.
        <a href="/practice-areas/wills-and-estates-planning" title="Wills and estates planning">Find out more</a>.</p>
    <br>
    <h4>BUSINESS SUCCESSION PLANNING</h4>
    <p>Hunt &amp; Hunt advises on issues related to business succession planning and succession law. We prepare shareholder agreements and family and business structures, and advise on potential distributions under trusts. We also provide advice concerning challenges to wills under the family provisions arrangements in accordance with relevant succession legislation. </p>
