<div class="container">
                <h3>NEWS, EVENTS AND PUBLICATIONS</h3>
                <div class="row">
                                          <div class="col-sm-3">
                        <div>
                            <img src="./Home Page_files/news-placeholder.jpg" alt="publications">
                            <h4 style="text-transform:uppercase">No liability for home-turf topple: McKenzie v Day (No 2) [2016] NSWDC 236 </h4>
                            <p>In the District Court of NSW the plaintiff claimed damages for negligence hav...</p>
                            <a href="https://hunthunt.worldsecuresystems.com/news-publications/no-liability-for-home-turf-topple-mckenzie-v-day-no-2-2016-nswdc-236" title="Read More">READ MORE &gt;</a>
                        </div>
                        <p><a class="btn  button-default" href="https://hunthunt.worldsecuresystems.com/publications" role="button">SEE ALL PUBLICATIONS</a>
                        </p>
                    </div>
                                        <div class="col-sm-3">
                        <div>
                            <img src="./Home Page_files/news-placeholder.jpg" alt="publications">
                            <h4 style="text-transform:uppercase">Out of touch but not out of time </h4>
                            <p>In a decision [Mackenzie v Positive Concepts Pty Ltd &amp; Anon [2016] VSC 259 (1...</p>
                            <a href="https://hunthunt.worldsecuresystems.com/news-publications/out-of-touch-but-not-out-of-time" title="Read More">READ MORE &gt;</a>
                        </div>
                        <p><a class="btn  button-default" href="https://hunthunt.worldsecuresystems.com/publications" role="button">SEE ALL PUBLICATIONS</a>
                        </p>
                    </div>
                                            <div class="col-sm-3">
                        <div>
                            <img src="./Home Page_files/news-placeholder.jpg" alt="news">
                            <h4 style="text-transform:uppercase">Title News 1 </h4>
                            <p></p><p>As foreshadowed in my e-alert of yesterday, the Australian Trade Minister ...</p>
                            <a href="https://hunthunt.worldsecuresystems.com/news-1/news-1" title="Read More">READ MORE &gt;</a>
                        </div>
                        <p><a class="btn  button-default" href="https://hunthunt.worldsecuresystems.com/news" role="button">SEE ALL NEWS</a>
                        </p>
                    </div>
                                            <div class="col-sm-3">
                        <div>
                            <img src="./Home Page_files/news-placeholder.jpg" alt="events">
                            <h4 style="text-transform:uppercase">Event 2 </h4>
                            <p></p><p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry...</p>
                            <a href="https://hunthunt.worldsecuresystems.com/BookingRetrieve.aspx?ID=60159" title="Read More">READ MORE &gt;</a>
                        </div>
                        <p><a class="btn  button-default" href="https://hunthunt.worldsecuresystems.com/events" role="button">SEE ALL EVENTS</a>
                        </p>
                    </div>
                                         <!--
			<div class="col-sm-3">
			 <div>
			  <img src="/img/news-placeholder.jpg" alt="news" />
			  <h4>EVENT 1 </h4>
			  <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper.</p>
			  <a href="#" title="Read More">READ MORE &gt;</a>
			 </div>
			 <p><a class="btn  button-default" href="#" role="button">SEE ALL EVENTS</a></p>
			</div>
			<div class="col-sm-3">
			 <div>
			  <img src="/img/news-placeholder.jpg" alt="news" />
			  <h4>PUBLICATION 1 </h4>
			  <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. </p>
			  <a href="#" title="Read More">READ MORE &gt;</a>
			  </div>
			  <p><a class="btn  button-default" href="#" role="button">SEE ALL PUBLICATION</a></p>
		   </div>
			<div class="col-sm-3">
			 <div>
			  <img src="/img/news-placeholder.jpg" alt="news" />
			  <h4>NEWS 1 </h4>
			  <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. </p>
			  <a href="#" title="Read More">READ MORE &gt;</a>
			 </div>
			 <p><a class="btn  button-default" href="#" role="button">SEE ALL NEWS</a></p>
			</div>
			<div class="col-sm-3">
			 <div>
			  <img src="/img/news-placeholder.jpg" alt="news" />
			  <h4>NEWS 2 </h4>
			  <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. </p>
			  <a href="#" title="Read More">READ MORE &gt;</a>
			 </div>
			 <p><a class="btn  button-default" href="#" role="button">SEE ALL NEWS</a></p>
			</div>-->
                </div>
            </div>