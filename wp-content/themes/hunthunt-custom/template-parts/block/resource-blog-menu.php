

<aside style="margin-top: 40px;">
    <div>
        <?php $parenttitle = "Recent News & Articles" ?>
        <h4 class="black-title"><?php echo $parenttitle ?></h4>
        <h4>
        <?php
        // the query
        $the_query = new WP_Query( array(
            'posts_per_page' => -1,
        )); 
        ?>

        <?php if ( $the_query->have_posts() ) : ?>
        <ul id="homepage-posts">
        <aside>
            <ul>
        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
            <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
        <?php endwhile; ?>
            </ul>
        </aside>
        <?php wp_reset_postdata(); ?>

        <?php else : ?>
        <p><?php __('No News'); ?></p>
        <?php endif; ?>
</h4></div>
</aside>