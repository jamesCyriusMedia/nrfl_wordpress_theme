<aside>
    <div>
        <h4 class="black-title">SERVICES</h4>
        <h4>

    <ul>
		<li class="<?php echo $page == 'sectors-aged'? 'submenu-selected' : ''?>"><a href="aged-care" title="Aged care">Aged care </a></li>
		<li class="<?php echo $page == 'sectors-agribusiness'? 'submenu-selected' : ''?>"><a href="agribusiness" title="Agribusiness">Agribusiness </a></li>
		<li class="<?php echo $page == 'sectors-alpine'? 'submenu-selected' : ''?>"><a href="alpine" title="Alpine">Alpine</a></li>
		<li class="<?php echo $page == 'sectors-automotive'? 'submenu-selected' : ''?>"><a href="automotive" title="Automotive">Automotive </a></li>
		<li class="<?php echo $page == 'sectors-banking'? 'submenu-selected' : ''?>"><a href="banking-and-finance" title="Banking and finance">Banking and finance </a></li>
		<li class="<?php echo $page == 'sectors-building'? 'submenu-selected' : ''?>"><a href="building-and-construction" title="Building and construction">Building and construction </a></li>
		<li class="<?php echo $page == 'sectors-business'? 'submenu-selected' : ''?>"><a href="business" title="Business">Business </a></li>
		<li class="<?php echo $page == 'sectors-customs'? 'submenu-selected' : ''?>"><a href="customs-and-global-trade" title="Customs and Global Trade">Customs and Global Trade </a></li>
		<li class="<?php echo $page == 'sectors-education'? 'submenu-selected' : ''?>"><a href="education" title="Education">Education</a></li>
		<li class="<?php echo $page == 'sectors-energy'? 'submenu-selected' : ''?>"><a href="energy-and-resources" title="Energy and resources">Energy and resources</a></li>
		<li class="<?php echo $page == 'sectors-government'? 'submenu-selected' : ''?>"><a href="government" title="Government">Government</a></li>
		<li class="<?php echo $page == 'sectors-health'? 'submenu-selected' : ''?>"><a href="health" title="Health">Health</a></li>
		<li class="<?php echo $page == 'sectors-insurance'? 'submenu-selected' : ''?>"><a href="insurance" title="Insurance">Insurance</a></li>
		<li class="<?php echo $page == 'sectors-not-for-profit'? 'submenu-selected' : ''?>"><a href="not-for-profit" title="Not-for-profit">Not-for-profit</a></li>		
		<li class="<?php echo $page == 'sectors-private'? 'submenu-selected' : ''?>"><a href="private-clients" title="Private clients">Private clients</a></li>		
    </ul>
</h4></div>
</aside>