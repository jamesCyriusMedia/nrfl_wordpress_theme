
    <p class="bigger-red-text">Our Australian family law experts bring clarity to complex family issues, providing advice and solutions that are unique to each client. We'll work hard to resolve your matter quickly and cost-effectively.</p>
    <p>At Hunt &amp; Hunt, our friendly team of family lawyers offers advice on issues related to families and domestic relationships. We understand that family disputes are difficult for everyone involved, so we work quickly to support you every step of the way.
    </p>
    <p>We have extensive experience in Australian family law services, including:
    </p>
    <ul>
        <li>child custody and support</li>
        <li>child relocation applications</li>
        <li>complex financial issues</li>
        <li>conveyancing and stamp duties</li>
        <li>court representation</li>
        <li>de facto relationships</li>
        <li>divorce</li>
        <li>domestic violence</li>
        <li>financial agreements</li>
        <li>pre-nuptial agreements</li>
        <li>property and financial settlements</li>
        <li>separation</li>
        <li>spouse maintenance</li>
        <li>tax</li>
        <li>wills and estate planning, including real estate.</li>
    </ul>
    <br>
    <h4>DIVISION OF PROPERTY AND RELATED COMMERCIAL MATTERS</h4>
    <p>We can advise you on related property and commercial matters, and assist with protecting your assets, including drafting wills and acting in contested-estate matters.
    </p>
    <br>
    <h4>FAMILY COURT APPEARANCES</h4>
    <p>Going to court to settle a family dispute is a stressful experience. We'll stay by your side and help minimise the strain by ensuring your case is strongly and effectively argued.
    </p>

