
    <p class="bigger-red-text">Hunt &amp; Hunt's insolvency team has built an excellent reputation for providing timely advice to insolvency practitioners and major creditors. We can help clients develop practical, commercial solutions regarding corporate administrations, liquidations and receiverships, and can assist with business recovery, reconstruction and restructuring strategies.</p>
    <p>Hunt &amp; Hunt provides insolvency administration services to public and private sector organisations including major accounting firms and their clients; automotive groups; banks and financial institutions; government agencies; insurers; and businesses in the IT and telecommunications industries.</p>

    <p>Our insolvency lawyers have many decades of practice and knowledge. The team's technical legal skills and commercial acumen have been put to good use by the Australian Taxation Office, which has used Hunt &amp; Hunt's insolvency services since 2005. Hunt &amp; Hunt's insolvency experts also sit on the External Advisory Panel of the Australian Securities and Investments Commission.</p>
    <br>
    <h4>PROTECTING YOUR INTERESTS</h4>
    <p>In the complex circumstances surrounding insolvencies, we work quickly and accurately to protect clients' interests and provide advice on:</p>
    <ul>
        <li>Bankruptcy</li>
        <li>Corporate reconstructions and restructuring</li>
        <li>Creditors' claims</li>
        <li>Cross-border insolvency</li>
        <li>Directors' duties</li>
        <li>Distress debt trading and acquisitions</li>
        <li>Enforcements</li>
        <li>Investigating accountants' reports</li>
        <li>Liquidations</li>
        <li>Negotiations with secured creditors</li>
        <li> Preference claims</li>
        <li>Receiverships</li>
        <li>Recovery strategies</li>
        <li>Securities</li>
        <li>Turnaround and workouts</li>
        <li>Voidable transactions</li>
        <li>Voluntary administration.</li>
    </ul>
    <p>Our practitioners closely monitor national and international industry developments, through membership with the Insolvency Practitioners Association, the Insolvency Committee of the Law Council of Australia, and the International Women's Insolvency Reconstruction Confederation.</p>

    <p class="literature-container"> <span class="icon"><img src="/img/pdf.png" alt="Insolvency" style="width:16px;height:16px;border:0px;"></span> <span class="name"><a href="/pdf/Insolvency.pdf">Insolvency</a></span> (2056 KB) </p>

