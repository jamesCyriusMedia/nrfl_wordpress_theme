<p class="bigger-red-text">
    In 1988, Hunt &amp; Hunt became one of the first foreign law firms to enter the Chinese market. Since then, our experience, community ties and relationships have continued to strengthen and grow.</p>
<p>From our offices in Shanghai, Sydney, Melbourne and Adelaide, we provide advice to clients pursuing trade and investment opportunities in Australia and China. </p>

<p>We have a clear understanding of the Chinese business environment and can help with the commercial and legal aspects of doing business in China. We have assisted investors from China in working through government processes, conducting due diligence on projects and advising on investment policies and procedures. Our expertise includes: drafting, reviewing and negotiating commercial contracts and other documents in English and Chinese treaties, conventions and practice concerning international trade and commerce property acquisition protection and licensing of intellectual property in Australia and overseas, including drafting, reviewing and negotiating technology licensing or transfer agreements taxation and other financial issues, including fundraising.</p>
<br>
<h4>CHINA DISPUTES AND ARBITRATION</h4>
<p>Hunt &amp; Hunt regularly works on commercial dispute matters in China involving litigation and arbitration – particularly debt litigation – for wholly owned foreign enterprises (WOFEs). The quality of our dispute resolution services in China is recognised by the Asia Pacific Legal 500.</p>
<br>
<h4>CHINA JOINT VENTURES</h4>
<p>We help Chinese businesses select and establish business partners, trading entities and joint ventures in Australia, as well as help establish equity and cooperative joint ventures, WOFEs and representative offices in China. We have assisted investors from China in joint ventures across a variety of industries including mining and resources, agriculture, health, aged care and property.</p>
<br>
<h4>MORE THAN JUST LEGAL ADVICE</h4>
<p>Our team provides government and commercial introductory services to help you explore Chinese trade and business opportunities. We conduct market research and feasibility studies, including the development of market entry strategies. We also provide English and Chinese language translation services in-house or by referral to external translators.</p>
<br>
<h4>FORGING STRONGER BUSINESS TIES WITH CHINA</h4>
<p>Our lawyers regularly participate in outbound business delegations to promote Australia's capabilities to the Chinese business community. Our partners attend trade missions to China, sponsored by the Australian federal and state governments. We also regularly host visiting Chinese delegations.</p>

<p>We work closely with the Commonwealth Attorney General's Department on issues relating to the Australia-China Free Trade Agreement, which affects law, banking and insurance.</p>

<p>Hunt &amp; Hunt is also a major sponsor and supporter of the Australian Institute of Export and is a member of the Australia China Business Council (ACBC). Partner Jim Harrowell is currently the New South Wales President of the ACBC and the firm is also represented on the ACBC Executive Committees in Victoria.</p>
<br>
<h4>AUSTRALIAN UNIVERSITIES IN CHINA</h4>
<p>We've helped several Australian universities enter the Chinese market, including the University of Ballarat, Edith Cowan University, Macquarie University, the University of South Australia and the University of Tasmania.
</p>
<br>
<h4 class="black-title">Credentials</h4>
<p>Hunt &amp; Hunt remains one of only six Australian firms licensed by China's Ministry of Justice to practice foreign law in China. </p>

<p>In June 2006, Jim Harrowell, Managing Partner of our Shanghai office, was appointed a Member of the Order of Australia in recognition of his contribution to forging and nurturing legal and business links with China.</p>

<p class="literature-container"> <span class="icon"><img src="/img/pdf.png" alt="China Advisory Brochure" style="width:16px;height:16px;border:0px;"></span> <span class="name"><a href="/pdf/ChinaAdvisoryEnglish.pdf">China Advisory Brochure</a></span> (1191 KB) </p>

