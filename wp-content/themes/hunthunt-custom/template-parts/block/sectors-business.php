

    <p class="bigger-red-text">We offer legal services to businesses of all sizes, including advice on asset protection and succession planning, debt litigation, property, employment and workplace relations, and planning and environment issues.</p>

    <p>From our offices around Australia and China, Hunt &amp; Hunt's corporate advisory specialists provide expert advice on a wide range of business issues, while taking into account all international, national, state and territory legislative requirements.
    </p>
    <p>Our expertise spans from starting a small business to the complex array of legal issues facing large listed corporations. We treat all client matters with equal care.
    </p>
    <p>We act as trusted advisors to our clients, building up knowledge about their businesses over time and continually increasing the value of the advice we provide. By quickly identifying and managing issues in an open and accessible manner, we help our clients remain competitive and agile.
    </p>
    <br>
    <h4>INTERNATIONAL SCOPE</h4>
    <p>Hunt &amp; Hunt's exclusive membership of Interlaw, an association of independent commercial law firms across 129 cities worldwide, gives our clients access to additional business expertise on the ground.
    </p>
    <p>We have particularly strong business ties with China. Our Shanghai-based team, led by Partner Jim Harrowell AM, collaborates with lawyers in our Australian offices to assist clients pursuing trade and investment opportunities in Australia and China.
    </p>

    <p class="literature-container"> <span class="icon"><img src="/img/pdf.png" alt="Commercial Advisory" style="width:16px;height:16px;border:0px;"></span> <span class="name"><a href="/pdf/CommercialAdvisory.pdf">Commercial Advisory</a></span> (2003 KB) </p>

