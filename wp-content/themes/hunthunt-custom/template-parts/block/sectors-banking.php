

    <p class="bigger-red-text">Our multidisciplinary team of banking and finance lawyers understand the intricacies of banking and finance law as well as their specialist markets.</p>
    <p>We work with many of Australia's leading organisations within the banking and financial services sectors, as well as several established international banks. Our experience in advising multiple stakeholders across different jurisdictions ensures the advice we provide is balanced, innovative and practical.
    </p>
    <br>
    <h4>SECURITY ENFORCEMENTS AND DEBT LITIGATION</h4>
    <p>Our Banking and Finance team has considerable experience in security enforcement, insolvency administration and debt recovery, across all Australian courts and jurisdictions. Find out more
    </p>
    <br>
    <h4>MORTGAGE DOCUMENTATION CENTRE</h4>
    <p>Our specialist Mortgage Documentation Centre prepares, checks and dispatches loan documentation within 24 hours of initial instruction. Find out more
    </p>
    <br>
    <h4>LIMITED RECOURSE BORROWING ARRANGEMENTS</h4>
    <p>We prepare limited recourse borrowing arrangement documentation for self-managed superannuation funds. Our banking and finance specialists can provide compliance certification for lenders, as well as compliant loan and security documentation.
    </p>
    <br>
    <h4>ISLAMIC FINANCE TRANSACTIONS</h4>
    <p>Hunt &amp; Hunt is one of a very few Australian law firms with expertise in Islamic finance transactions. Drawing on direct market experience, we can document, act on and advise on Sharia-compliant securities. Find out more
    </p>
    <br>
    <p class="literature-container"> <span class="icon"><img src="/img/pdf.png" alt="Banking and finance" style="width:16px;height:16px;border:0px;"></span> <span class="name"><a href="/pdf/BankingandFinance.pdf">Banking and finance</a></span> (2327 KB) </p>
    <br>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                              	Islamic finance transactions	

                            </a>
                          </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">

                    <p> Drawing on direct market experience, Hunt &amp; Hunt's Islamic finance law specialists can document, act and advise on Sharia-compliant securities.</p>
                    <p>As Australia's Muslim population continues to grow, there is increasing demand for Islamic banking and financial services.</p>

                    <p>Hunt &amp; Hunt is one of a very small number of law firms in Australia that has the expertise and capability to assist you with Islamic finance transactions. </p>

                    <p>For example, our lawyers recently completed a substantial syndicated facility for two financial institutions secured over a retirement village and aged care facility. </p>

                    <p>Our exclusive membership of Interlaw, an association of independent commercial law firms across 129 cities worldwide, gives us access to additional Islamic finance expertise.</p>

                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                              		Mortgage documentation centre	

                            </a>
                          </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">

                    <p>Our specialist Mortgage documentation centre prepares, checks and dispatches loan documentation within 24 hours of initial instruction. Each year, we produce more than 3,000 mortgage documents for banking and finance clients.
                    </p>
                    <p>Hunt &amp; Hunt's Mortgage documentation centre helps financial services providers quickly prepare documents for mortgages ranging from simple residential purchases to complex arrangements involving family trusts, guarantees and collateral documents.
                    </p>
                    <p>Our proprietary mortgage processing system and rigorous checks ensure mortgage documentation is ready 24 hours after we receive instructions from a client.
                    </p>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
                <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                              	Security enforcements and debt litigation	
                            </a>
                          </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">

                    <p>Our Banking and Finance team has considerable experience in security enforcement, insolvency administration and debt recovery across all Australian courts and jurisdictions.</p>
                    <p>Hunt &amp; Hunt's national team helps clients recover and resolve difficult or disputed debts. We offer legal insolvency services in relation to debt recovery, security enforcement and all aspects of insolvency administration.
                    </p>
                    <p>We help our clients enforce residential and commercial mortgage defaults, recover arrears and obtain freezing orders.

                    </p>
                    <h4>Debt litigation services for organisations of all sizes</h4>
                    <p>In 2005, Hunt &amp; Hunt became the only private law firm appointed by the Australian Taxation Office (ATO) to provide national debt litigation services. We were reappointed to this position in 2010. We conduct a large number of bankruptcy and winding-up matters on behalf of the ATO.
                    </p>
                    <p>To assist all of our debt litigation clients, we draw on our experience and the same business processes we use to assist the ATO. This ensures our clients have their matters resolved efficiently and cost-effectively. It's important to us that our clients receive the same level of meticulous, cost-conscious service, regardless of size.
                    </p>
                </div>
            </div>
        </div>

    </div>
