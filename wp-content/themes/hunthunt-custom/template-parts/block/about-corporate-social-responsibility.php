

    <p class="bigger-red-text">As a major Australian firm, we believe it's our responsibility to minimise our environmental impact, and to give our time and skills to benefit the communities in which we work.</p>
    <br>
    <h4>MINIMISING OUR ECOLOGICAL FOOTPRINT</h4>
    <p>Hunt &amp; Hunt is a proud member of the <a href="http://www.legalsectoralliance.com.au/" target="_blank" title="Australian Legal Sector Alliance ">Australian Legal Sector Alliance (AusLSA)</a> – an industry-led association that promotes sustainable practices within the legal sector. As a member of AusLSA, we: </p>
    <ol>
        <li>measure, manage and reduce our environmental impact </li>
        <li>work with external stakeholders to reduce our indirect environmental impact </li>
        <li>promote awareness of sustainability across our business </li>
        <li>participate in public debate on sustainability to develop, apply and promote best practice across the legal sector </li>
        <li>report on our progress</li>
    </ol>
    <br>
    <p> We report annually on our environmental policies and achievements using the AusLSA environmental consumption calculator. This measures greenhouse gas emissions, electricity, business travel data, paper, water and waste, and carbon-mitigating activities.</p>
    <br>
    <h4>CLEAN UP AUSTRALIA</h4>
    <p>An integral part of our environmental strategy is our support of <a href="http://www.cleanup.org.au/au/" target="_blank" title="Clean Up Australia">Clean Up Australia</a> – a national not-for-profit organisation that works with communities to conserve our environment. We have been providing pro bono commercial legal advice to Clean Up Australia since 1988.</p>
    <br>
    <h4>THE BANKSIA ENVIRONMENTAL FOUNDATION</h4>
    <p>Hunt &amp; Hunt also supports the <a href="http://www.banksiafdn.com/" target="_blank" title="Banksia Environmental Foundation">Banksia Environmental Foundation</a> – a national, not-for-profit organisation that recognises and rewards environmental excellence.</p>
    <br>
    <h4>HELPING THOSE WHO NEED IT MOST</h4>
    <p>Pro bono work is a vital part of our firm's culture. Hunt &amp; Hunt encourages and supports its people to use their skills to help those who have limited resources, or who can't access legal assistance when they need it. </p>
    <p> In addition to our work with Clean Up Australia, Hunt &amp; Hunt provides pro bono services to a number of charities. Our recent pro bono work includes acting for disability discrimination complainants, participating in ethics review committees for health organisations, and sitting on boards for not-for-profit organisations.</p>
