<aside>
    <div>
        <?php $parenttitle = get_the_title($post->post_parent); ?>
        <h4 class="black-title"><?php echo $parenttitle ?></h4>
        <h4>
    <ul>
        <?php $page_url = wp_make_link_relative(get_permalink()) ?>
        <!-- <?php echo $page_url ?> -->
        <li class="<?php echo $page_url == '/about-us/'? 'submenu-selected' : ''?>"><a href="/about-us" title="Who We Are ">About Hunt & Hunt North Ryde</a></li>
        <li class="<?php echo $page_url == '/about-us/our-people/'? 'submenu-selected' : ''?>"><a href="/about-us/our-people" title="Our People">Our People</a></li>
        <li class="<?php echo $page_url == '/about-us/membership-association/'? 'submenu-selected' : ''?>"><a href="/about-us/membership-association" title="Memberships & Associations">Memberships & Associations</a></li>
    </ul>
</h4></div>
</aside>