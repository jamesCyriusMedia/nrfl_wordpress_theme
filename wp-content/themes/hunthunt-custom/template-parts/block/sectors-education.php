

    <p class="bigger-red-text">Hunt &amp; Hunt has extensive experience acting for secondary and tertiary institutions in Australia and overseas. Clients value our commercial and cultural advice on complex joint ventures, exchange programs, and credit recognition for academic technology transfer in Australia, China, Southeast Asia and the Indian subcontinent.</p>
    <p>Our lawyers are well-respected in the education sector and provide advice to more than 40 universities, colleges and schools.</p>

    <p>We have long-standing relationships with Austrade, Australian state departments of education, and local and international universities and TAFEs. This involvement gives us a detailed understanding of industry issues.</p>
    <br>
    <h4>COMPREHENSIVE LEGAL SERVICES FOR THE EDUCATION SECTOR</h4>
    <p>Hunt &amp; Hunt advises organisations and institutions on joint venture arrangements and establishing offshore campuses; domestic and international intellectual property issues; property acquisition, planning and leasing; and employment law. </p>

    <p>We provide practical, ongoing advice on the legal and managerial obligations for domestic and international programs; and the interpretation of education stakeholders' responsibilities under the Education Services for Overseas Students (ESOS) regime and National Code of Practice.</p>

    <p>Hunt &amp; Hunt assists educational institutions with:</p>
    <ul>
        <li>debt recovery matters</li>
        <li>drafting contracts</li>
        <li>foreign credit recognition agreements</li>
        <li>governance, policy and compliance issues</li>
        <li>privacy</li>
        <li>property</li>
    </ul>

