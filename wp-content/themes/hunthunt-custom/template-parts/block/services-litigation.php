
    <p class="bigger-red-text">Hunt &amp; Hunt's litigation and dispute resolution experts can help you resolve commercial disputes with minimal impact on your business.</p>

    <p>Our team of litigation and dispute resolution specialists understand the stress of litigation and the effect it can have on the resources and financial health of your organisation. </p>

    <p>We provide practical advice and devise litigation strategies that enable you to realistically determine your chances of success. Our objective is always to help you avoid disputes and costly court cases. Before launching into the litigation process, we'll apply our mediation and negotiation skills to secure an early, cost effective resolution.</p>

    <p>Hunt &amp; Hunt's litigation team differs from other practices. Our lawyers regularly appear as counsel, which means we don't need to spend time briefing counsel. This effectively reduces time to litigation and client costs.</p>

    <p>We specialise in:</p>
    <ul>
        <li>administrative law</li>
        <li>arbitration</li>
        <li>commissions and inquiries</li>
        <li>competition and trade practices litigation</li>
        <li>construction disputes</li>
        <li>consumer protection</li>
        <li>commercial disputes</li>
        <li>insolvency and debt litigation</li>
        <li>insurance</li>
        <li>defamation</li>
        <li>directors and officers disputes</li>
        <li>employment disputes</li>
        <li>environmental and planning law</li>
        <li>intellectual property disputes</li>
        <li>property law</li>
        <li> regulatory compliance</li>
    </ul>
    <br>
    <h4>WHO WE WORK FOR</h4>
    <p>Our clients come from the corporate, private and public sectors and represent many industries. We also act for several Australian Government and state government agencies.</p>

    <br>
    <h4>PREVENTION STRATEGIES TO REDUCE RISK</h4>
    <p>In the past, companies viewed dispute resolution as a reactive process, but smart businesses now realise they can implement prevention and risk management strategies to protect against costly litigation. We have helped many clients create practical dispute resolution procedures that form part of their corporate risk prevention and management strategies.</p>

    <br>
    <h4>COMMERCIAL DISPUTE RESOLUTION</h4>
    <p>Our dispute resolution specialists help devise cost-effective and practical outcomes that are directed towards the early resolution of disputes. These processes will often include mediation. We keep clients involved throughout the dispute resolution process, including the planning and risk-assessment phases.
    </p>

    <p>We ensure our clients are actively involved throughout the planning and risk-assessment stages of their disputes. This ensures that realistic goals are set and the best commercial outcomes are achieved.</p>

    <br>
    <h4>COMMERCIAL LITIGATION</h4>
    <p>Hunt and Hunt's lawyers handle many litigation and arbitration matters from single-issue injunction cases to multi-million dollar disputes. We have managed cases at every level of the court system, including state supreme courts, the Federal Court of Australia and The High Court of Australia.</p>

    <br>
    <h4>DEBT LITIGATION</h4>
    <p>We have extensive experience in debt litigation, and practise in all Australian courts and jurisdictions. Our litigation and dispute resolution specialists work with banks and other financial institutions as well as public and private sector organisations. </p>
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                              Find out more
                            </a>
                          </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    <p>With our national network of specialist debt litigation lawyers, Hunt &amp; Hunt has the resources, technology and expertise to cost-effectively manage debt litigation anywhere in Australia.
                    </p>
                    <p>Hunt &amp; Hunt's litigation and dispute resolution specialists are experienced in all aspects of debt litigation. We provide services to banks and other financial institutions as well as public and private sector organisations, including major accounting firms. Our litigation lawyers practice in all courts and jurisdictions, often as advocates.
                    </p>
                    <p>Hunt &amp; Hunt aims to strike a balance between pursuing debtors and encouraging compromise where appropriate. We take great care to protect your reputation when recovering monies from your customers.
                    </p>

                    <h4>Debt litigators for the Australian Taxation Office</h4>
                    <p>In 2005, Hunt &amp; Hunt was the only private law firm appointed to provide debt litigation services nationally to the Australian Taxation Office (ATO). We were reappointed to this position in 2010.
                    </p>

                    <h4>Expert litigation lawyers with a national focus</h4>
                    <p>Our litigation lawyers work across borders to provide a seamless service to our clients throughout Australia and the Asia-Pacific region. They pride themselves on providing clear and concise explanations of legal principles and tactics.
                    </p>
                    <p>We also advise on: </p>
                    <ul>
                        <li>insolvency matters, including title claims, preference claims and priority disputes
                        </li>
                        <li>deeds of company arrangement
                        </li>
                        <li>enforcing securities
                        </li>
                        <li>renegotiating ongoing contacts with receivers and managers
                        </li>
                        <li>all forms of insolvency and debt litigation.
                        </li>
                    </ul>

                </div>
            </div>
        </div>

    </div>

