

    <p class="bigger-red-text">Hunt &amp; Hunt acts for major automotive dealers across Australia, providing legal advice on operational issues and corporate compliance.</p>
    <p>Australia's automotive industry is one of the most open and competitive in the world. In the face of industry challenges such as tightening regulations, increased competition and fluctuating fuel and materials costs, many of the country's largest automotive dealers turn to Hunt &amp; Hunt for fast and accurate legal advice. </p>

    <p>To keep pace with these issues as they arise, our industry specialists work quickly to identify what needs to be done to protect dealers' interests. We regularly advise dealers on operational issues such as commercial leasing, supply agreements, employment issues, franchise and consumer law.
    </p>
    <br>
    <h4>DEALERSHIP ACQUISITIONS AND SALES</h4>
    <p>To negotiate the best possible purchase or sales terms, our industry specialists act on dealership acquisitions and sales as an extension of clients' management teams. We also assist clients in financing and negotiations with automotive lenders and manufacturers, and with business succession and restructuring issues.
    </p>
    <p>Our automotive lawyers specialise in the unique challenges of dealership properties, from contamination issues to the upgrade requirements of vehicle manufacturers.
    </p>
    <p>To keep our clients up to date with legal issues and industry developments, Hunt &amp; Hunt conducts regular presentations and roundtable discussions for its automotive clients throughout the year. We also send out regular legal alerts and notifications on issues affecting the automotive industry.
    </p>
    <br>
    <p class="literature-container"> <span class="icon"><img src="/img/pdf.png" alt="Automotive" style="width:16px;height:16px;border:0px;"></span> <span class="name"><a href="/pdf/Automotive.pdf">Automotive</a></span> (1844 KB) </p>

